// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "NNSCharacter.h"
#include "Perception/PawnSensingComponent.h"
#include "BaseEnemyCharacter.generated.h"

class UAIAsyncTaskBlueprintProxy;
class UAITask_MoveTo;

/**
 * Base class for all enemy characters. Blueprints should subclass this.
 */
UCLASS()
class NEWNEWSORPIGAL_API ABaseEnemyCharacter : public ANNSCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABaseEnemyCharacter(const FObjectInitializer& ObjectInitializer);

	virtual void SaveLoadCommon(FArchive& Archive) override;
	virtual void Save(FArchive& Archive) override;
	virtual void Load(FArchive& Archive, bool bIgnoreTransforms = false) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	virtual void EndTurn() override;
	virtual void StartTurn() override;

	virtual void OnDeath() override;
	virtual void UpdateWalkSpeed() override;

	UFUNCTION(BlueprintCallable, Category = "AI")
	void FinishAttack();

	UPROPERTY(EditDefaultsOnly, Category = "AI")
	UAnimMontage* AttackAnimation;

	UFUNCTION(BlueprintPure, Category = "AI")
	bool CanCastSpell() const;

	UFUNCTION(BlueprintCallable, Category = "AI")
	void CastRandomSpell();

	TArray<ESpells> GetCastableSpells(bool bExitAfterFirst) const;

	virtual void Attack() override;

	virtual void DealDamage(UDamageData* Damage, bool DamageAll = false) override;
	virtual void DealDamage(UDamageData* Damage, UCharacterSheet* Character) override;

	// How much experience the enemy is worth
	UFUNCTION()
	int32 GetExperience() const;

	// Callback after death animation
	UFUNCTION()
	void OnDeathAnimationFinished();

	// Callback after we turn physics on to make enemy fall through floor
	UFUNCTION()
	void OnFallThroughFloorFinished();

	// Class used to spawn loot after death
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	TSubclassOf<class APickup> LootPickupClass;

	// Key to load from EnemyDataTable
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	FName DataTableName;

	UPROPERTY(BlueprintReadOnly, Category = Gameplay)
	FVector SpawnLocation;

	UPROPERTY()
	EStatusEffects StatusOnHit;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI")
	UPawnSensingComponent* SensingComponent;
	
	UFUNCTION()
	void OnSeePawn(APawn* Pawn);

	UFUNCTION()
	void OnHearPawn(APawn* Pawn, const FVector& Location, float Volume);

	UPROPERTY(BlueprintReadOnly, Category = "AI")
	int32 TimeLastNoticed;

	UPROPERTY(BlueprintReadOnly, Category = "AI")
	FVector LocationLastNoticed;

	UPROPERTY(BlueprintReadOnly, Category = "AI")
	bool bLastNoticedSight;

	UPROPERTY()
	int32 TimeCanCastNextSpellAt;

	UPROPERTY(BlueprintReadOnly, Category = "AI")
	float MaxWalkDistance;

	UPROPERTY()
	bool bStartedTurnModeInRadius;
private:
	void OnTurnLimitExpired();

	UPROPERTY()
	FTimerHandle TurnLimitHandle;
};
