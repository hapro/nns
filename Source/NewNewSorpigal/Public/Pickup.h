// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Pickup.generated.h"

class APlayerPartyCharacter;
class UItem;

UCLASS()
class NEWNEWSORPIGAL_API APickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickup(const FObjectInitializer& ObjectInitializer);

	void SaveLoadCommon(FArchive& Archive);
	void Save(FArchive& Archive);
	void Load(FArchive& Archive);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION()
	void AddToInventory(APlayerPartyCharacter* Player);

	UFUNCTION()
	void OnDisablePhysics();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UBoxComponent* BoxComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	int32 Gold;

	UPROPERTY()
	UItem* Item;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	bool bCreateNewItem;

	UFUNCTION(BlueprintImplementableEvent, category = "Gameplay")
	UItem* CreateItem();
};
