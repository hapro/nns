// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseEnemyCharacter.h"
#include "BaseNPCCharacter.generated.h"

class UDialogComponent;

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API ABaseNPCCharacter : public ABaseEnemyCharacter
{
	GENERATED_BODY()
	
public:
	ABaseNPCCharacter(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

	virtual void StartTurn();

	UPROPERTY(VisibleAnywhere, Category = "Dialog")
	UDialogComponent* DialogComponent;

	UPROPERTY(EditAnywhere, Category = "Dialog")
	USoundWave* Greeting;
};
