// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "StaticInteractable.h"
#include "Globals.h"
#include "StatContest.generated.h"

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API AStatContest : public AStaticInteractable
{
	GENERATED_BODY()
	
public:
	AStatContest(const FObjectInitializer& ObjectInitializer);

	virtual void SaveLoadCommon(FArchive& Archive) override;

	virtual void Interact(ANNSCharacter* Character) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Gameplay")
	int32 SkillPoints;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Gameplay")
	int32 RequiredStat;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Gameplay")
	EStats StatTested;

	UPROPERTY()
	TArray<bool> AlreadyWon;

	UPROPERTY()
	USoundWave* WinSound;

private:
	bool AllWon() const;
};
