// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "StaticInteractable.h"
#include "Globals.h"
#include "ShopBuilding.generated.h"

class ANNSCharacter;
class UItem;

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API AShopBuilding : public AStaticInteractable
{
	GENERATED_BODY()
	
public:
	AShopBuilding(const FObjectInitializer& ObjectInitializer);

	virtual void SaveLoadCommon(FArchive& Archive) override;
	virtual void Save(FArchive& Archive) override;
	virtual void Load(FArchive& Archive) override;

	virtual void BeginPlay() override;

	virtual void Interact(ANNSCharacter* Character) override;
	
	UPROPERTY(EditAnywhere, Category = "Shop")
	EShopTypes ShopType;

	UPROPERTY(EditAnywhere, Category = "Shop")
	int32 Tier;

	UPROPERTY()
	TArray<UItem*> Items;

	UPROPERTY(EditAnywhere, Category = "Shop")
	USoundWave* Greeting;

	UPROPERTY(EditAnywhere, Category = "Shop")
	USoundWave* Goodbye;

private:
	int32 NumItems;

	int32 LastTier;
};
