// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UItem;

/**
 * Generates random items of the requested tier
 */
class NEWNEWSORPIGAL_API ItemGenerator
{
public:
	ItemGenerator();
	~ItemGenerator();

	UItem* GenerateItem(int32 ItemLevel);
	UItem* GenerateWeapon(int32 ItemLevel);
	UItem* GenerateArmorChest(int32 ItemLevel);
	UItem* GenerateArmorOther(int32 ItemLevel);
	UItem* GenerateSpellbook(int32 ItemLevel);
	UItem* GeneratePotion(int32 ItemLevel);

private:
	UItem* GenerateEquippableItem(const FName& Name);

	enum ItemType {
		ItemWeapon,
		ItemArmor,
		ItemGold,
		ItemSpellbook,
		ItemPotion,
		ItemTotalItemTypes
	};

	TMap<ItemType, TMap<int32, TArray<FString>>> Database;

	static const int32 kEnchantChance;
};
