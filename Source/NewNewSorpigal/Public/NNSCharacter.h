// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Globals.h"
#include "NNSCharacter.generated.h"

class NNSCharacter;
class AProjectile;
class UCharacterSheet;
class UDamageData;

/**
 * Base Character class for all Characters in NNS
 * Stores an array of UCharacterSheets which represent the
 * Character's party. The party could be of size 1.
 */
UCLASS(abstract)
class NEWNEWSORPIGAL_API ANNSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ANNSCharacter(const FObjectInitializer& ObjectInitializer);

	virtual void SaveLoadCommon(FArchive& Archive);
	virtual void Save(FArchive& Archive);
	virtual void Load(FArchive& Archive, bool bIgnoreTransforms = false);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// MUST call this function if in real-time mode, as the timers will crash when they expire otherwise
	void CustomDestroy();
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Attack based on currently-equipped weapon and enemy distance
	UFUNCTION(BlueprintCallable, Category = Gameplay)
	virtual void Attack() {}

	// Pass turn. If in realtime mode, simply advances to next character
	UFUNCTION(BlueprintCallable, Category = Gameplay)
	virtual void Pass();

	// When a PartyMember takes a turn, set the current active member to the next slot
	UFUNCTION()
	void AdvanceActiveCharacter();

	// Whether to check mesh collision after a capsule collision. True unless Character has no mesh
	UFUNCTION()
	virtual bool ShouldCheckMeshCollision() const { return true; }

	// Deal damage to a random PartyMember, or all of them
	virtual void DealDamage(UDamageData* Damage, bool DamageAll = false);
	virtual void DealDamage(UDamageData* Damage, UCharacterSheet* Character);
	virtual void OnDeath() {}
	
	// Update bIsDead if all PartyMembers are dead
	UFUNCTION()
	virtual void CheckIsCharacterDead();

	virtual void UpdateWalkSpeed() {}

	// Can any party member currently act
	UFUNCTION(BlueprintCallable, Category = Gameplay)
	bool CanMemberAct();

	// When switching from Real->Turn
	UFUNCTION()
	void ConvertTimerToTurnCount();

	// When switching from Turn->Real
	UFUNCTION()
	void ConvertTurnCountToTimer();

	// Initializes turn in turn-based mode
	UFUNCTION()
	virtual void StartTurn();

	// Finishes turn in turn-based mode. Call whenever a PartyMember performs an action
	UFUNCTION()
	virtual void EndTurn();

	virtual void AddStatusEffect(EPartyStatusEffects Status, int32 Duration = StatusEffectConstants::kMaxDuration, int32 Strength = 1);
	virtual void RemoveStatusEffect(EPartyStatusEffects Status);
	void TickStatusEffects(int32 Duration);
	bool HasStatus(EPartyStatusEffects Status) { return StatusEffects.Contains(Status); }

	UFUNCTION()
	virtual void OnMovementModeChanged(EMovementMode PrevMovementMode, uint8 PrevCustomMode) override;

	// For all conscious characters, add their stats together to a single value
	UFUNCTION()
	int32 GetPartyCombinedStat(EStats Stat) const;

	// For all conscious characters, find the one with the highest skill level
	UFUNCTION()
	void GetPartySkill(ESkills Skill, int32& Level, EMastery& Mastery) const;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay)
	TArray<UCharacterSheet*> PartyMembers;

	// Where to spawn projectiles from
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Gameplay)
	UArrowComponent* ProjectileSpawnComponent;
	
	// PartyMember whose turn is up
	UPROPERTY(BlueprintReadWrite, Category = Gameplay)
	int32 ActiveCharacter;

	UPROPERTY(BlueprintReadOnly, Category = Gameplay)
	bool bIsDead;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	float MaxMeleeDistance;

	UPROPERTY()
	TMap<EPartyStatusEffects, FStatusEffect> StatusEffects;

	// Only used in turn-based mode
	UPROPERTY()
	bool bIsMyTurn;

	UPROPERTY()
	float RegularJumpHeight;

	UPROPERTY()
	float RegularAcceleration;

	UPROPERTY()
	float FallPeak;

	UPROPERTY(EditDefaultsOnly, Category = "Audio")
	TArray<USoundWave*> AttackSounds;

	UPROPERTY(EditDefaultsOnly, Category = "Audio")
	TArray<USoundWave*> DamageSounds;

	UPROPERTY(EditDefaultsOnly, Category = "Audio")
	TArray<USoundWave*> DeathSounds;

	int32 CharacterIndex(UCharacterSheet* Character) const { return PartyMembers.IndexOfByKey(Character); }

	void PlaySound(TArray<USoundWave*>& Sounds, int32 Index);

	static const float PassTime;

	// Aim target when launching projectile at this character
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Gameplay)
	UArrowComponent* ProjectileTargetComponent;

	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AProjectile> ArrowClass;
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AProjectile> FireBallClass;
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AProjectile> FlameBoltClass;
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AProjectile> ShockClass;
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AProjectile> CauseLightClass;
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AProjectile> CauseHeavyClass;

	// Direction of untargeted ranged attacks
	UFUNCTION()
	virtual FVector GetProjectileSpawnDirection() const;

	UFUNCTION()
	AProjectile* SpawnProjectile(TSubclassOf<class AProjectile>& ProjectileClass);

	UFUNCTION()
	void LaunchProjectile(AProjectile* Projectile, ANNSCharacter* Target = nullptr);

	// Magic spells
	typedef bool (ANNSCharacter::*SpellFunctionType)(UCharacterSheet*);
	TMap<ESpells, SpellFunctionType> SpellFunctions;

	virtual bool CastSpell(ESpells Spell, UCharacterSheet* Target = nullptr);

	bool CastTorchLight(UCharacterSheet* Target = nullptr);
	bool CastFireShield(UCharacterSheet* Target = nullptr);
	bool CastFlameBolt(UCharacterSheet* Target = nullptr);
	bool CastFireSpikes(UCharacterSheet* Target = nullptr);
	bool CastFireBall(UCharacterSheet* Target = nullptr);
	bool CastWizardsEye(UCharacterSheet* Target = nullptr);
	bool CastAirShield(UCharacterSheet* Target = nullptr);
	bool CastShock(UCharacterSheet* Target = nullptr);
	bool CastFeatherFall(UCharacterSheet* Target = nullptr);
	bool CastWaterWalk(UCharacterSheet* Target = nullptr);
	bool CastJump(UCharacterSheet* Target = nullptr);
	bool CastFly(UCharacterSheet* Target = nullptr);
	bool CastTeleportTown(UCharacterSheet* Target = nullptr);
	bool CastCureLight(UCharacterSheet* Target = nullptr);
	bool CastHeroism(UCharacterSheet* Target = nullptr);
	bool CastCauseLight(UCharacterSheet* Target = nullptr);
	bool CastFlee(UCharacterSheet* Target = nullptr);
	bool CastRegen(UCharacterSheet* Target = nullptr);
	bool CastCureHeavy(UCharacterSheet* Target = nullptr);
	bool CastCauseHeavy(UCharacterSheet* Target = nullptr);
};
