// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "StaticInteractable.h"
#include "Globals.h"
#include "Pool.generated.h"

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API APool : public AStaticInteractable
{
	GENERATED_BODY()
	
public:
	APool(const FObjectInitializer& ObjectInitializer);

	virtual void SaveLoadCommon(FArchive& Archive) override;

	virtual void Interact(ANNSCharacter* Character) override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UStaticMeshComponent* Liquid;

	UPROPERTY(EditAnywhere, Category = "Gameplay")
	EStats Stat;

	UPROPERTY(EditAnywhere, Category = "Gameplay")
	int32 Amount;

	UPROPERTY(EditAnywhere, Category = "Gameplay")
	int32 TimesUsable;

	UPROPERTY(EditAnywhere, Category = "Gameplay")
	int32 MaxAllowed;

	UPROPERTY()
	USoundWave* DrinkSound;
};
