// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "Globals.h"
#include "NNSGameInstance.generated.h"

class ANNSHUD;
class ItemGenerator;
class UCharacterSheet;
class UQuestManager;
class UTimeOfDayManager;

USTRUCT()
struct FSkillsToMastery
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	TMap<ESkills, EMastery> Masteries;
};

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API UNNSGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	UNNSGameInstance(const FObjectInitializer& ObjectInitializer);

	virtual void Shutdown() override;
	virtual void Init() override;
	
	// Player characters, stored here to persist across levels
	UPROPERTY()
	TArray<UCharacterSheet*> PlayerPartyMembers;

	UPROPERTY()
	uint32 PlayerGold;

	static UDataTable* EquippableItemDataTable;
	static UDataTable* PotionItemDataTable;
	static UDataTable* EnemyDataTable;
	static UDataTable* QuestItemDataTable;

	UPROPERTY()
	UParticleSystem* BloodParticles;

	// Switch between real/turn modes
	UFUNCTION()
	bool ToggleTurnMode();

	bool SetTurnMode(bool bEnable, bool bForce = false);

	// When a character finishes their move in turn-based mode, call this to update turn order
	UFUNCTION()
	void EndTurn();

	UFUNCTION()
	void NotifyDeath(UCharacterSheet* Character);

	UPROPERTY(BlueprintReadOnly, Category = "Gameplay")
	bool bIsInTurnMode;

	UPROPERTY(BlueprintReadWrite, Category = "Gameplay")
	bool bIsIndoors;

	UPROPERTY()
	FStreamableManager AssetLoader;

	UFUNCTION()
	UTexture* GetTexture(TAssetPtr<UTexture>& Texture);

	UFUNCTION()
	USoundWave* GetSound(TAssetPtr<USoundWave>& Sound);

	UPROPERTY(BlueprintReadOnly, Category = "Gameplay")
	UQuestManager* QuestManager;

	UPROPERTY(BlueprintReadOnly, Category = "Gameplay")
	UTimeOfDayManager* TimeOfDayManager;

	TSharedPtr<ItemGenerator> ItemGeneratorObject;

	TWeakObjectPtr<ANNSHUD> HUD;

	static const int32 kCharactersInParty = 4;

	UFUNCTION()
	void InsertPlayerIntoTurnOrder(UCharacterSheet* Member);

	UFUNCTION()
	void InsertIntoTurnOrder(UCharacterSheet* Member);

	void ShowWireframeColors(bool bShowColors);

	UFUNCTION(BlueprintCallable, Category = "SaveLoad")
	void SaveGameToFile(const FString& FileName);
	UFUNCTION(BlueprintCallable, Category = "SaveLoad")
	void LoadGameFromFile(const FString& FileName);

	UFUNCTION(BlueprintCallable, Category=SaveLoad)
	static TArray<FString> GetSaveFiles(bool bIgnoreAutoQuick = false);

	static TMap<FName, UClass*> ClassMap;
	static UClass* GetClassFromMap(const FName& Name);

	UFUNCTION(BlueprintCallable, Category = "SaveLoad")
	void OpenLevel(const FName& LevelName);

	UFUNCTION(BlueprintCallable, Category = "SaveLoad")
	void SwitchLevel(const FName& LevelName);
	UFUNCTION(BlueprintCallable, Category = "SaveLoad")
	void Teleport(const FName& LevelName, const FVector& Location, const FRotator& Rotation);
	UFUNCTION(BlueprintCallable, Category = "SaveLoad")
	void TeleportToObject(const FName& LevelName, const FName& OtherSideObjectName);

	UFUNCTION(BlueprintCallable, Category = "SaveLoad")
	void LoadCurrentLevel();

	UFUNCTION(BlueprintCallable, Category = "SaveLoad")
	void QuitToMainMenu();

	void Respawn();

	UFUNCTION(BlueprintCallable, Category = "SaveLoad")
	void BeginGame(TArray<FCharacterCreatorInfo> Characters);

	UPROPERTY()
	UParticleSystem* FireSpikeParticles;

	UFUNCTION(BlueprintCallable, Category = "Audio")
	UAudioComponent* PlayMusic(USoundBase* Sound, float FadeTime = 0.0f);

	UFUNCTION(BlueprintCallable, Category = "Audio")
	void StopMusic(UAudioComponent* Sound, float FadeTime = 0.0f);

	UFUNCTION(BlueprintCallable, Category = "Audio")
	void PlaySoundEffect2D(USoundBase* Sound);

	UFUNCTION(BlueprintCallable, Category = "Audio")
	void PlaySoundEffect3D(USoundBase* Sound, const FVector& Location);

	UFUNCTION(BlueprintCallable, Category = "Audio")
	void RegisterAmbientSound(UAudioComponent* Sound);

	UFUNCTION(BlueprintCallable, Category = "Audio")
	void SetMasterVolume(float Volume);
	UFUNCTION(BlueprintCallable, Category = "Audio")
	float GetMasterVolume() const { return MasterVolume; }
	UFUNCTION(BlueprintCallable, Category = "Audio")
	void SetMusicVolume(float Volume);
	UFUNCTION(BlueprintCallable, Category = "Audio")
	float GetMusicVolume() const { return MusicVolume; }
	UFUNCTION(BlueprintCallable, Category = "Audio")
	void SetSoundEffectVolume(float Volume);
	UFUNCTION(BlueprintCallable, Category = "Audio")
	float GetSoundEffectVolume() const { return SoundEffectVolume; }

	UFUNCTION()
	void AdjustPlayingVolumes(bool bMusic, bool bSounds);

	UPROPERTY(BlueprintReadWrite, Category = "Options")
	float MotionBlurAmount;

	UPROPERTY(BlueprintReadWrite, Category = "Options")
	float MouseSensitivity;

	UPROPERTY(BlueprintReadWrite, Category = "Options")
	bool bUseCombatMode;

	UFUNCTION(BlueprintCallable, Category = "Options")
	void SetFieldOfView(int32 FOV);

	UFUNCTION(BlueprintCallable, Category = "Options")
	static int32 GetFieldOfView() { return FieldOfView; }

	UFUNCTION(BlueprintCallable, Category = "Options")
	void SetUseBackwardsWalkSpeed(bool bUse);

	UFUNCTION(BlueprintPure, Category = "Options")
	bool UseBackwardsWalkSpeed() const { return bUseBackwardsSpeed; }

	TWeakObjectPtr<ASceneCapture2D> MinimapCam;

	static TMap<EClass, FSkillsToMastery> ClassSkillMap;

	static void GetLevelBoundingBox(UWorld* World, bool bIsIndoors, FVector& Origin, FVector& Extent);

	UPROPERTY(BlueprintReadOnly, Category = "UI")
	TArray<UTexture2D*> Portraits;

	UPROPERTY(BlueprintReadWrite, Category = "Options")
	bool bDrawHotkeysOnScreen;

private:
	UPROPERTY()
	float MasterVolume;

	UPROPERTY()
	float MusicVolume;

	UPROPERTY()
	float SoundEffectVolume;

	UPROPERTY()
	TArray<UAudioComponent*> PlayingMusic;

	UPROPERTY()
	TArray<UAudioComponent*> PlayingSounds;

	UPROPERTY()
	TArray<TWeakObjectPtr<UCharacterSheet>> TurnOrder;

	static int32 FieldOfView;

	UPROPERTY()
	bool bUseBackwardsSpeed;

	// Advances turn order until a character can act, then starts turn
	UFUNCTION()
	void CheckTurnOrder();

	void RespawnPrivate();

	UFUNCTION()
	void InitSpell(ESpells Spell, const FString& Name, int32 Mana, EMastery Mastery, float Delay, ESkills Skill, ETargetMode Target, const FString& Texture, const FString& Sound);

	void InitPartyMembers(const TArray<FCharacterCreatorInfo>& Characters);

	void SaveLoadCommon(FArchive& Archive);
	void Save(FArchive& Archive);
	void SaveCurrentLevel();
	void Load(FArchive& Archive);

	// Current cache of saved data
	TMap<FString, FBufferArchive> SavedLevels;
	FBufferArchive SavedPlayer;
	bool bIgnorePlayerTransforms;
	bool bTeleportOnLoad;
	FVector TeleportLocation;
	FRotator TeleportRotation;
	FName TeleportObject;

	// User dir
	static const FString BaseSaveDir;
	static const FString SaveFilePattern;

	static const int32 QuicksaveSlots;
	static const int32 AutosaveSlots;
};
