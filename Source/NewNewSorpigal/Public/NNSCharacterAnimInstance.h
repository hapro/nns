// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Animation/AnimInstance.h"
#include "NNSCharacterAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API UNNSCharacterAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
public:
	UNNSCharacterAnimInstance(const FObjectInitializer& ObjectInitializer);
	
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Anim")
	bool bIsDead;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Anim")
	bool bIsMoving;
};
