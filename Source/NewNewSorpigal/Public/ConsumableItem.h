// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Item.h"
#include "ConsumableItem.generated.h"

class UCharacterSheet;

/**
 * Abstract base class for items that can be "used". Must implement Consume()
 */
UCLASS()
class NEWNEWSORPIGAL_API UConsumableItem : public UItem
{
	GENERATED_BODY()
	
public:
	UConsumableItem();
	
	virtual bool Consume(UCharacterSheet* Character) { return false; }
	
};
