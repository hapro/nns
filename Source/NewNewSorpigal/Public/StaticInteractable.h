// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "StaticInteractable.generated.h"

class ANNSCharacter;

UCLASS()
class NEWNEWSORPIGAL_API AStaticInteractable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStaticInteractable(const FObjectInitializer& ObjectInitializer);

	virtual void SaveLoadCommon(FArchive& Archive);
	virtual void Save(FArchive& Archive);
	virtual void Load(FArchive& Archive);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Interactable")
	void InteractBlueprint(ANNSCharacter* Character);

	UFUNCTION(BlueprintCallable, Category = "Interactable")
	virtual void Interact(ANNSCharacter* Character) { InteractBlueprint(Character); }

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UBoxComponent* BoxComponent;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UStaticMeshComponent* Mesh;
};
