// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "StaticInteractable.h"
#include "TempleBuilding.generated.h"

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API ATempleBuilding : public AStaticInteractable
{
	GENERATED_BODY()
	
public:
	ATempleBuilding(const FObjectInitializer& ObjectInitializer);

	virtual void Interact(ANNSCharacter* Character) override;

	UPROPERTY(EditAnywhere, Category = "Gameplay")
	float Multiplier;
};
