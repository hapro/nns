// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ConsumableItem.h"
#include "Spellbook.generated.h"

class UCharacterSheet;

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API USpellbook : public UConsumableItem
{
	GENERATED_BODY()
	
public:
	USpellbook();

	virtual void SaveLoadCommon(FArchive& Archive) override;

	UFUNCTION(BlueprintCallable, Category = "ItemCreate")
	static USpellbook* Create(ESpells Spell);

	virtual bool MatchesShop(EShopTypes ShopType) override;

	virtual TArray<FString> ToString() const override;

	virtual bool Consume(UCharacterSheet* Character);

	virtual uint32 GetBuyCost() const override;
	
	UPROPERTY()
	ESpells Spell;

	UPROPERTY()
	EMastery Mastery;

	UPROPERTY()
	ESkills Skill;

	virtual void Init() override;

	UPROPERTY()
	USoundWave* SpellLearnSound;

private:
	// Icons to use for each tier of spellbook
	static const FStringAssetReference BasicIcon;
	static const FStringAssetReference ExpertIcon;
	static const FStringAssetReference MasterIcon;
};
