// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "StaticInteractable.h"
#include "QuestInteractable.generated.h"

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API AQuestInteractable : public AStaticInteractable
{
	GENERATED_BODY()
	
public:
	AQuestInteractable(const FObjectInitializer& ObjectInitializer);

	virtual void SaveLoadCommon(FArchive& Archive) override;

	virtual void Interact(ANNSCharacter* Character) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Gameplay")
	EQuestFlags Quest;	
};
