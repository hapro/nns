// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "StaticInteractable.h"
#include "Globals.h"
#include "StatusPedestal.generated.h"

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API AStatusPedestal : public AStaticInteractable
{
	GENERATED_BODY()
	
public:
	AStatusPedestal(const FObjectInitializer& ObjectInitializer);

	virtual void Interact(ANNSCharacter* Character) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Gameplay")
	int32 Duration;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Gameplay")
	int32 Strength;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Gameplay")
	EStatusEffects Status;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Gameplay")
	EPartyStatusEffects PartyStatus;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UStaticMeshComponent* OrbMesh;

	UPROPERTY()
	USoundWave* Sound;
};
