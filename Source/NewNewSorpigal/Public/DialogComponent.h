// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "Globals.h"
#include "DialogComponent.generated.h"

USTRUCT(BlueprintType)
struct FDialogLine
{
	GENERATED_USTRUCT_BODY()

public:
	FDialogLine() {
		Flag = EQuestFlags::QuestTotalQuests;
		FlagNum = 0;
	}

	// Key to access dialog
	UPROPERTY(EditAnywhere, Category = "Dialog")
	FName Key;

	// Dialog displayed in text box
	UPROPERTY(EditAnywhere, Category = "Dialog", meta = (MultiLine = "true"))
	FText Dialog;

	// Dialog displayed in text box if the quest is finished
	UPROPERTY(EditAnywhere, Category = "Dialog", meta = (MultiLine = "true"))
	FText AltDialog;

	// Quest flag to trigger when dialog is read
	UPROPERTY(EditAnywhere, Category = "Dialog")
	EQuestFlags Flag;

	UPROPERTY(EditAnywhere, Category = "Dialog")
	int32 FlagNum;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class NEWNEWSORPIGAL_API UDialogComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDialogComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UPROPERTY(EditAnywhere, Category = "Dialog")
	FText Greeting;

	UPROPERTY(EditAnywhere, Category = "Dialog")
	TArray<FDialogLine> DialogLines;

	UPROPERTY(EditAnywhere, Category = "Dialog")
	ESkills Skill;
};
