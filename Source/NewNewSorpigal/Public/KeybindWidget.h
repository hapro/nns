// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "KeybindWidget.generated.h"

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API UKeybindWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UKeybindWidget(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintPure, Category = "Keys")
	static TArray<FInputActionKeyMapping> GetBindings();
};
