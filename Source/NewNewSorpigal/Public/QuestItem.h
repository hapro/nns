// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Item.h"
#include "QuestItem.generated.h"

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API UQuestItem : public UItem
{
	GENERATED_BODY()
	
public:
	UQuestItem();
	
	UFUNCTION(BlueprintCallable, Category = "ItemCreate")
	static UQuestItem* Create(const FName& BaseName);

	virtual void Init() override;
	
};
