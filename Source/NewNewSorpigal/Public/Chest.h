// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "StaticInteractable.h"
#include "Globals.h"
#include "Chest.generated.h"

class ANNSCharacter;
class UInventory;

UCLASS()
class NEWNEWSORPIGAL_API AChest : public AStaticInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AChest(const FObjectInitializer& ObjectInitializer);

	virtual void SaveLoadCommon(FArchive& Archive) override;
	virtual void Save(FArchive& Archive) override;
	virtual void Load(FArchive& Archive) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Spawns items in the chest, if it hasn't already happened
	UFUNCTION()
	void InitItems();

	// Inits the chest, and then opens the HUD
	virtual void Interact(ANNSCharacter* Character) override;

	UPROPERTY()
	UInventory* Inventory;

	UPROPERTY()
	bool bIsInitialized;

	UPROPERTY(EditAnywhere, Category = "Trap")
	ETrapTypes Trap;

	UPROPERTY(EditAnywhere, Category = "Trap")
	ETrapLevel TrapLevel;

	UPROPERTY(EditAnywhere, Category = "Items")
	int32 Tier;

	UPROPERTY(EditAnywhere, Category = "Items")
	int32 NumItems;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Gameplay")
	UParticleSystem* FireTrapParticles;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Gameplay")
	UParticleSystem* AirTrapParticles;

	UPROPERTY()
	USoundWave* OpenSound;

private:
	static const int32 kExpertPenalty;
	static const int32 kMasterPenalty;

	TMap<ETrapTypes, UParticleSystem*> ParticleMap;
	TMap<ETrapTypes, USoundWave*> SoundMap;
};
