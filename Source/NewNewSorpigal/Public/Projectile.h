// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

class ANNSCharacter;
class UDamageData;

UCLASS()
class NEWNEWSORPIGAL_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Virtual callback for OnOverlapBegin
	virtual void OverlapBeginCallback(AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// Virtual callback for OnHit
	virtual void HitCallback(AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	// When base implementation of OverlapBeginCallback detects a collision with a character's mesh, this is called
	virtual void OnOverlapMesh(ANNSCharacter* HitCharacter);
	virtual void OnOverlapMesh(ANNSCharacter* HitCharacter, FHitResult& Hit);

	UFUNCTION()
	void InitVelocity(const FVector& ShootDirection);

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
	USphereComponent* CollisionComp;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
	UStaticMeshComponent* Mesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
	UProjectileMovementComponent* ProjectileMovement;

	// Damage dealt on collision
	UPROPERTY()
	UDamageData* Damage;

	UPROPERTY()
	float DamageRadius;
	
	// Sound to play when InitVelocity() is called
	UPROPERTY(EditDefaultsOnly, Category = "Audio")
	USoundWave* Sound;

private:
	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
};
