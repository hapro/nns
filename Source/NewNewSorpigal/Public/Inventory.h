// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "Inventory.generated.h"

class UCharacterSheet;
class UItem;

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API UInventory : public UObject
{
	GENERATED_BODY()

public:
	UInventory();

	void Save(FArchive& Archive);
	void Load(FArchive& Archive);

	// Helper functions
	UItem* GetItem(int32 X, int32 Y) const;
	static int32 GetIndex(int32 X, int32 Y);

	// Search left to right, top to bottom for enough open slots to fit the item
	UFUNCTION()
	bool AddItemToFirstOpenSlot(UItem* Item);

	UFUNCTION()
	bool RemoveItem(UItem* Item);

	UFUNCTION()
	bool CanDropOrSwap(int32 StartX, int32 StartY, UItem* Item, UItem*& SwapItem) const;

	// Call CanDropOrSwap, then performs the appropriate action if able to
	UFUNCTION()
	bool DropOrSwap(int32 StartX, int32 StartY, UItem* Item, UItem*& SwapItem);
	
	// Since Unreal doesn't do 2D with TArray, this is an array of Width*Height size. Use GetItem() or GetIndex()
	UPROPERTY()
	TArray<UItem*> Items;

	// Map to store the top-left corner of every item in the inventory
	TMap<TWeakObjectPtr<UItem>, TPair<int32, int32>> PositionMap;

	// Inventory size
	static const int32 kMaxWidth;
	static const int32 kMaxHeight;

private:
	// If all spaces are empty, returns nullptr. Otherwise, returns pointer to first object hit
	UFUNCTION()
	UItem* IsSpaceEmpty(int32 StartX, int32 StartY, int32 Width, int32 Height) const;

	// Forces the item in the requested slot. Call IsSpaceEmpty() or CanDropOrSwap() first
	UFUNCTION()
	bool PutItemInSlot(int32 StartX, int32 StartY, UItem* Item);

	static const int32 kArrayLength;
};

inline UItem* UInventory::GetItem(int32 X, int32 Y) const
{
	return Items[GetIndex(X, Y)];
}

inline int32 UInventory::GetIndex(int32 X, int32 Y)
{
	return Y * kMaxWidth + X;
}
