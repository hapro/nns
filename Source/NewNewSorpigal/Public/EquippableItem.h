// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Item.h"
#include "Globals.h"
#include "EquippableItem.generated.h"

struct FEnemyData;
struct FEquippableItemData;

/**
 * 
 */
UCLASS()
class NEWNEWSORPIGAL_API UEquippableItem : public UItem
{
	GENERATED_BODY()
	
public:
	UEquippableItem();

	static UEquippableItem* Create(const FEnemyData& Data);
	UFUNCTION(BlueprintCallable, Category = "ItemCreate")
	static UEquippableItem* Create(const FName& BaseName);

	virtual void Init() override;

	virtual void SaveLoadCommon(FArchive& Archive) override;

	virtual bool MatchesShop(EShopTypes ShopType) override;

	void Enchant();

	// Returns info about damage, delay, etc.
	virtual TArray<FString> ToString() const override;

	virtual uint32 GetBuyCost() const override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay)
	EEquipType Slot;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay)
	EEquipStyle Style;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	int32 Dice;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	int32 DiceSides;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	int32 Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	float Delay;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	int32 Armor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	TArray<int32> StatModifiers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	TArray<int32> SkillModifiers;
};
