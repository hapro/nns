// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataTable.h"
#include "Globals.h"
#include "PotionItem.h"
#include "DataStructs.generated.h"

class UCharacterSheet;
class APickup;

UENUM(BlueprintType)
enum class EDamageDistance : uint8
{
	DamageMelee,
	DamageRanged,
	DamageInstant
};

USTRUCT(BlueprintType)
struct FCharacterCreatorInfo
{
	GENERATED_USTRUCT_BODY()

public:
	FCharacterCreatorInfo() {Portrait = nullptr;}

	UPROPERTY(BlueprintReadWrite, Category = "Gameplay")
	int32 Strength;

	UPROPERTY(BlueprintReadWrite, Category = "Gameplay")
	int32 Intelligence;

	UPROPERTY(BlueprintReadWrite, Category = "Gameplay")
	int32 Dexterity;

	UPROPERTY(BlueprintReadWrite, Category = "Gameplay")
	int32 Speed;

	UPROPERTY(BlueprintReadWrite, Category = "Gameplay")
	int32 Vitality;

	UPROPERTY(BlueprintReadWrite, Category = "Gameplay")
	int32 Luck;

	UPROPERTY(BlueprintReadWrite, Category = "Gameplay")
	FString Name;

	UPROPERTY(BlueprintReadWrite, Category = "Gameplay")
	EClass Class;

	UPROPERTY(BlueprintReadWrite, Category = "Gameplay")
	UTexture2D* Portrait;

	UPROPERTY(BlueprintReadWrite, Category = "Gameplay")
	TArray<ESkills> Skills;

	UPROPERTY(BlueprintReadWrite, Category = "Gameplay")
	int32 Voice;
};

UCLASS()
class UDamageData : public UObject
{
	GENERATED_BODY()

public:
	UDamageData() : Super(), DamageType(EDamageTypes::DamagePhysical), DamageAmount(0.0f), Accuracy(-1), DamageDistance(EDamageDistance::DamageMelee), DamageSource(TEXT("")), SourceActor(nullptr), bIsRetaliation(false)
	{
		Status.Key = EStatusEffects::StatusTotalStatusEffects;
	}

	static UDamageData* Create(EDamageTypes Type, float Amount, float Accuracy, EDamageDistance Distance, FString Source, AActor* SourceActor = nullptr, bool bIsRetaliation = false,
							   EStatusEffects Status = EStatusEffects::StatusTotalStatusEffects, const FStatusEffect& Effect = FStatusEffect())
	{
		UDamageData* Damage = NewObject<UDamageData>();
		Damage->DamageType = Type;
		Damage->DamageAmount = Amount;
		Damage->Accuracy = Accuracy;
		Damage->DamageDistance = Distance;
		Damage->DamageSource = Source;
		Damage->SourceActor = SourceActor;
		Damage->bIsRetaliation = bIsRetaliation;
		Damage->Status.Key = Status;
		Damage->Status.Value = Effect;
		return Damage;
	}

	UPROPERTY()
	EDamageTypes DamageType;

	UPROPERTY()
	float DamageAmount;

	UPROPERTY()
	float Accuracy;

	UPROPERTY()
	EDamageDistance DamageDistance;

	UPROPERTY()
	FString DamageSource;

	UPROPERTY()
	AActor* SourceActor;

	UPROPERTY()
	bool bIsRetaliation;

	TPair<EStatusEffects, FStatusEffect> Status;
};

USTRUCT(BlueprintType)
struct FEquippableItemData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:

	FEquippableItemData() {}

	UPROPERTY()
	EEquipType Slot;

	UPROPERTY()
	int32 Tier;

	UPROPERTY()
	EEquipStyle Style;

	UPROPERTY()
	int32 Dice;

	UPROPERTY()
	int32 DiceSides;

	UPROPERTY()
	int32 Damage;

	UPROPERTY()
	float Delay;

	UPROPERTY()
	int32 Armor;

	UPROPERTY()
	TSubclassOf<class APickup> PickupClass;

	UPROPERTY()
	TAssetPtr<UTexture> Icon;

	UPROPERTY()
	int32 IconWidth;

	UPROPERTY()
	int32 IconHeight;
};

USTRUCT(BlueprintType)
struct FEnemyData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:

	FEnemyData() {}

	UPROPERTY()
	int32 Level;

	UPROPERTY()
	int32 Experience;

	UPROPERTY()
	float MaxSpeed;

	UPROPERTY()
	float MaxHealth;

	UPROPERTY()
	float MaxMana;

	UPROPERTY()
	float BaseDelay;

	UPROPERTY()
	int32 GoldMin;

	UPROPERTY()
	int32 GoldMax;

	UPROPERTY()
	EStatusEffects Status;

	UPROPERTY()
	int32 Dice;

	UPROPERTY()
	int32 DiceSides;

	UPROPERTY()
	int32 Damage;

	UPROPERTY()
	int32 Accuracy;

	UPROPERTY()
	int32 Evasion;

	UPROPERTY()
	int32 Armor;

	UPROPERTY()
	int32 Strength;

	UPROPERTY()
	int32 Intelligence;

	UPROPERTY()
	int32 Dexterity;

	UPROPERTY()
	int32 Speed;

	UPROPERTY()
	int32 Vitality;

	UPROPERTY()
	int32 Luck;

	UPROPERTY()
	int32 ResistFire;

	UPROPERTY()
	int32 ResistAir;

	UPROPERTY()
	TArray<ESpells> SpellList;

	UPROPERTY()
	int32 FireMagicSkill;

	UPROPERTY()
	EMastery FireMagicMastery;

	UPROPERTY()
	int32 AirMagicSkill;

	UPROPERTY()
	EMastery AirMagicMastery;

	UPROPERTY()
	int32 BodyMagicSkill;

	UPROPERTY()
	EMastery BodyMagicMastery;
};

USTRUCT(BlueprintType)
struct FPotionData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:

	FPotionData() {}

	UPROPERTY()
	EPotionTypes Enum;

	UPROPERTY()
	int32 Tier;

	UPROPERTY()
	EPotionTypes Item1;

	UPROPERTY()
	EPotionTypes Item2;

	UPROPERTY()
	TAssetPtr<UTexture> Texture;

	UPROPERTY()
	FString Tooltip;
};

USTRUCT(BlueprintType)
struct FClassData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY()
	int32 Sword;

	UPROPERTY()
	int32 Axe;

	UPROPERTY()
	int32 Dagger;

	UPROPERTY()
	int32 Bow;

	UPROPERTY()
	int32 Shield;

	UPROPERTY()
	int32 Leather;

	UPROPERTY()
	int32 Chain;

	UPROPERTY()
	int32 Plate;

	UPROPERTY()
	int32 Alchemy;

	UPROPERTY()
	int32 Armsmaster;

	UPROPERTY()
	int32 Bodybuilding;

	UPROPERTY()
	int32 DisarmTrap;

	UPROPERTY()
	int32 IdentifyItem;

	UPROPERTY()
	int32 IdentifyMonster;

	UPROPERTY()
	int32 Learning;

	UPROPERTY()
	int32 Meditation;

	UPROPERTY()
	int32 Merchant;

	UPROPERTY()
	int32 Swimming;

	UPROPERTY()
	int32 FireMagic;

	UPROPERTY()
	int32 AirMagic;

	UPROPERTY()
	int32 BodyMagic;
};

USTRUCT(BlueprintType)
struct FQuestItemData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:

	FQuestItemData() {}

	UPROPERTY()
	TSubclassOf<class APickup> PickupClass;

	UPROPERTY()
	TAssetPtr<UTexture> Icon;

	UPROPERTY()
	int32 IconWidth;

	UPROPERTY()
	int32 IconHeight;
};
