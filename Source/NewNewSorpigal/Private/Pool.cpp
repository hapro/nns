// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "Pool.h"

#include "CharacterSheet.h"
#include "DataStructs.h"
#include "NNSCharacter.h"
#include "NNSGameInstance.h"
#include "NNSHUD.h"

APool::APool(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	static ConstructorHelpers::FObjectFinder<USoundWave> DrinkFinder(TEXT("SoundWave'/Game/Sounds/fountain_drink.fountain_drink'"));
	DrinkSound = DrinkFinder.Object;

	Liquid = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("LiquidComp"));
	Liquid->BodyInstance.SetCollisionProfileName(FName(TEXT("NoCollision")));
	Liquid->CanCharacterStepUpOn = ECB_No;
	Liquid->bGenerateOverlapEvents = false;
	Liquid->SetNotifyRigidBodyCollision(false);
	Liquid->SetEnableGravity(false);
	Liquid->SetupAttachment(RootComponent);

	Stat = EStats::StatTotalStats;
	Amount = 1;
	TimesUsable = -1;
	MaxAllowed = 999999;
}

void APool::SaveLoadCommon(FArchive & Archive)
{
	Super::SaveLoadCommon(Archive);

	Archive << TimesUsable;
}

void APool::Interact(ANNSCharacter* Character)
{
	Super::Interact(Character);
	if (TimesUsable != 0) {
		if (DrinkSound) {
			Cast<UNNSGameInstance>(GetGameInstance())->PlaySoundEffect2D(DrinkSound);
		}

		UCharacterSheet* ActiveCharacter = Character->PartyMembers[Character->ActiveCharacter];
		if (Stat == EStats::StatCurrentHealth && ActiveCharacter->CurrentHealth < MaxAllowed) {
			if (Amount >= 0) {
				ActiveCharacter->Heal((float)Amount);
			} else {
				UDamageData* Damage = UDamageData::Create(EDamageTypes::DamagePure, Amount * -1.0f, -1, EDamageDistance::DamageInstant, TEXT("liquid"), this, true);
				ActiveCharacter->TakeDamage(Damage);
			}
		} else if (Stat == EStats::StatCurrentMana) {
			ActiveCharacter->UpdateCurrentMana(Amount);
		} else if (ActiveCharacter->GetBaseStat(Stat) < MaxAllowed) {
			ActiveCharacter->ChangeBaseStat(Amount, Stat);
		} else {
			Cast<UNNSGameInstance>(GetGameInstance())->HUD->Log(TEXT("Refreshing, but no effect"));
		}
		
		if (TimesUsable > 0) {
			--TimesUsable;
			if (TimesUsable == 0) {
				Tags.Empty();
			}
		}
	}
}
