// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "NNSGameViewportClient.h"

#include "NNSPlayerController.h"

UNNSGameViewportClient::UNNSGameViewportClient(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bMouseWasLocked = false;
}

void UNNSGameViewportClient::LostFocus(FViewport* InViewport)
{
	Super::LostFocus(InViewport);

	if (GetWorld()) {
		ANNSPlayerController* Controller = Cast<ANNSPlayerController>(GetWorld()->GetFirstPlayerController());
		if (Controller) {
			bMouseWasLocked = Controller->bLockMouseInCenter;
			Controller->bLockMouseInCenter = false;
		}
	}
}

void UNNSGameViewportClient::ReceivedFocus(FViewport* InViewport)
{
	Super::ReceivedFocus(InViewport);

	if (bMouseWasLocked) {
		if (GetWorld()) {
			ANNSPlayerController* Controller = Cast<ANNSPlayerController>(GetWorld()->GetFirstPlayerController());
			if (Controller) {
				Controller->bLockMouseInCenter = true;
			}
		}
		bMouseWasLocked = false;
	}
}
