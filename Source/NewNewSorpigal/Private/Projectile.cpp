// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "Projectile.h"

#include "DataStructs.h"
#include "NNSCharacter.h"
#include "NNSGameInstance.h"


// Sets default values
AProjectile::AProjectile(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	CollisionComp = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(0.5f);
	CollisionComp->BodyInstance.SetCollisionProfileName(FName(TEXT("Projectile")));
	CollisionComp->bGenerateOverlapEvents = true;
	CollisionComp->CanCharacterStepUpOn = ECB_No;
	// RigidBodyCollision == Hit event
	CollisionComp->SetNotifyRigidBodyCollision(true);
	CollisionComp->SetEnableGravity(false);
	// Callbacks for collisions
	CollisionComp->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::OnOverlapBegin);
	CollisionComp->OnComponentHit.AddDynamic(this, &AProjectile::OnHit);
	RootComponent = CollisionComp;

	Mesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("MeshComp"));
	Mesh->BodyInstance.SetCollisionProfileName(FName(TEXT("NoCollision")));
	Mesh->CanCharacterStepUpOn = ECB_No;
	Mesh->bGenerateOverlapEvents = false;
	Mesh->SetNotifyRigidBodyCollision(false);
	Mesh->SetEnableGravity(false);
	Mesh->SetupAttachment(RootComponent);

	ProjectileMovement = ObjectInitializer.CreateDefaultSubobject<UProjectileMovementComponent>(this, TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->MaxSpeed = 800.0f;
	ProjectileMovement->ProjectileGravityScale = 0.0f;

	InitialLifeSpan = 5.0f;

	Damage = nullptr;
	// A damage radius of 1.0 indicates a single-target attack. Override with larger values for AOE.
	DamageRadius = 1.0f;
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProjectile::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AProjectile::InitVelocity(const FVector& ShootDirection)
{
	ProjectileMovement->Velocity = ShootDirection * ProjectileMovement->MaxSpeed;
	if (Sound != nullptr) {
		Cast<UNNSGameInstance>(GetGameInstance())->PlaySoundEffect3D(Sound, GetActorLocation());
	}
}

void AProjectile::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	OverlapBeginCallback(OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
}

void AProjectile::OverlapBeginCallback(AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Can't shoot yourself
	// TODO: Can't shoot actors with same tag? Of same type?
	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr && OtherActor != GetOwner()) {
		if (OtherActor->IsA(ANNSCharacter::StaticClass())) {
			ANNSCharacter* Character = Cast<ANNSCharacter>(OtherActor);
			// This shouldn't happen because we turn off collision of dead enemies, but just in case
			if (Character->bIsDead) return;

			// If character has a mesh, check if the projectile hits the mesh. Otherwise, just assume it hits
			if (Character->ShouldCheckMeshCollision()) {
				const FVector StartTrace = CollisionComp->GetComponentLocation();
				const float CollisionDepth = Character->GetCapsuleComponent()->GetScaledCapsuleRadius() * 2.0f;
				const FVector EndTrace = StartTrace + CollisionComp->GetForwardVector() * CollisionDepth;

				FHitResult Hit(ForceInit);
				FCollisionQueryParams LineTraceParams(TEXT("LineTraceParams"), true);
				LineTraceParams.AddIgnoredComponent(OtherComp);
				// Line trace in the direction the arrow was moving to see if we hit the skeletal mesh
				if (GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, Character->GetMesh()->GetCollisionObjectType(), LineTraceParams)) {
					// Move arrow to location on skeletal mesh that it hit
					ProjectileMovement->StopMovementImmediately();
					CollisionComp->SetWorldLocation(Hit.Location);

					OnOverlapMesh(Character, Hit);
				}
			} else {
				OnOverlapMesh(Character);
			}
		}
	}
}

void AProjectile::OnOverlapMesh(ANNSCharacter* HitCharacter)
{
	if (Damage != nullptr) {
		if (DamageRadius > 1.0f) {
			// Deal damage to all characters in radius
			TArray<FOverlapResult> AllActorsInRadius;
			FCollisionObjectQueryParams Params(FCollisionObjectQueryParams::InitType::AllDynamicObjects);
			if (GetWorld()->OverlapMultiByObjectType(AllActorsInRadius, GetActorLocation(), FQuat::Identity, Params, FCollisionShape::MakeSphere(DamageRadius))) {
				for (auto& OverlapResult : AllActorsInRadius) {
					// Only count collision component, not mesh
					if (!OverlapResult.GetComponent()->IsA<UShapeComponent>()) continue;

					AActor* Actor = OverlapResult.GetActor();
					if (Actor->IsA<ANNSCharacter>()) {
						Cast<ANNSCharacter>(Actor)->DealDamage(Damage, true);
					}
				}
			}
		} else {
			HitCharacter->DealDamage(Damage);
		}
	}
	GetWorld()->DestroyActor(this);
}

void AProjectile::OnOverlapMesh(ANNSCharacter* HitCharacter, FHitResult& Hit)
{
	OnOverlapMesh(HitCharacter);
}

void AProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	HitCallback(OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectile::HitCallback(AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr) {
		GetWorld()->DestroyActor(this);
	}
}
