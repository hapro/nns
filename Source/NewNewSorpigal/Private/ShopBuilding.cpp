// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "ShopBuilding.h"

#include "CharacterSheet.h"
#include "Item.h"
#include "ItemGenerator.h"
#include "NNSCharacter.h"
#include "NNSGameInstance.h"
#include "NNSHUD.h"

AShopBuilding::AShopBuilding(const FObjectInitializer & ObjectInitializer)
	: Super(ObjectInitializer)
{
	ShopType = EShopTypes::ShopWeapon;
	Tier = -1;
	LastTier = -1;
	NumItems = 1;
	Greeting = nullptr;
	Goodbye = nullptr;
}

void AShopBuilding::SaveLoadCommon(FArchive & Archive)
{
	Super::SaveLoadCommon(Archive);

	Archive << LastTier;
}

void AShopBuilding::Save(FArchive & Archive)
{
	Super::Save(Archive);

	int32 NonNullItems = 0;
	for (auto& Item : Items) {
		if (Item == nullptr) continue;
		++NonNullItems;
	}

	Archive << NonNullItems;

	for (int32 i = 0; i < NumItems; ++i) {
		if (Items[i] == nullptr) continue;
		Archive << i;
		Items[i]->Save(Archive);
	}
}

void AShopBuilding::Load(FArchive & Archive)
{
	Super::Load(Archive);

	int32 NonNullItems = 0;
	Archive << NonNullItems;

	Items.Empty();
	Items.AddZeroed(NumItems);
	for (int32 i = 0; i < NumItems; ++i) {
		Items[i] = nullptr;
	}
	for (int32 i = 0; i < NonNullItems; ++i) {
		int32 Index = 0;
		Archive << Index;
		Items[Index] = UItem::Load(Archive);
	}
}

void AShopBuilding::BeginPlay()
{
	Super::BeginPlay();

	switch (ShopType) {
	case EShopTypes::ShopWeapon:
		NumItems = 6;
		break;
	case EShopTypes::ShopArmorChest:
		NumItems = 4;
		break;
	case EShopTypes::ShopArmorOther:
		NumItems = 8;
		break;
	case EShopTypes::ShopSpellbook:
		NumItems = 8;
		break;
	case EShopTypes::ShopAlchemy:
		NumItems = 8;
		break;
	}
	Items.Empty();
	Items.AddZeroed(NumItems);
	for (int32 i = 0; i < NumItems; ++i) {
		Items[i] = nullptr;
	}
}

void AShopBuilding::Interact(ANNSCharacter* Character)
{
	Super::Interact(Character);

	int32 GenerateTier = Tier;
	if (GenerateTier <= 0) {
		for (const UCharacterSheet* Member : Character->PartyMembers) {
			if (Member->Level > GenerateTier) {
				GenerateTier = Member->Level / 5 + 1;
			}
		}
	}

	if (GenerateTier != LastTier) {
		LastTier = GenerateTier;

		TSharedPtr<ItemGenerator> Generator = Cast<UNNSGameInstance>(GetGameInstance())->ItemGeneratorObject;
		Items.Empty();

		const TArray<int32> ProbabilityTiers = {FMath::Max(1, GenerateTier - 1), GenerateTier, GenerateTier, GenerateTier, FMath::Min(5, GenerateTier + 1)};

		for (int32 i = 0; i < NumItems; ++i) {
			UItem* Item = nullptr;
			for (int32 j = 0; Item == nullptr && j < 40; ++j) {
				const int32 RandomTier = ProbabilityTiers[FMath::RandRange(0, ProbabilityTiers.Num() - 1)];
				switch (ShopType) {
				case EShopTypes::ShopWeapon:
					Item = Generator->GenerateWeapon(RandomTier);
					break;
				case EShopTypes::ShopArmorChest:
					Item = Generator->GenerateArmorChest(RandomTier);
					break;
				case EShopTypes::ShopArmorOther:
					Item = Generator->GenerateArmorOther(RandomTier);
					break;
				case EShopTypes::ShopSpellbook:
					Item = Generator->GenerateSpellbook(RandomTier);
					break;
				case EShopTypes::ShopAlchemy:
					Item = Generator->GeneratePotion(RandomTier);
					break;
				}
			}
			Items.Add(Item);
			if (Item != nullptr) {
				Item->bIsIdentified = true;
			}
		}
	}
	Cast<UNNSGameInstance>(GetGameInstance())->HUD->EnterBuilding(this);
	if (Greeting != nullptr) {
		Cast<UNNSGameInstance>(GetGameInstance())->PlaySoundEffect2D(Greeting);
	}
}
