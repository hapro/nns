// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "OptionsWidget.h"

UOptionsWidget::UOptionsWidget(const FObjectInitializer & ObjectInitializer)
	: Super(ObjectInitializer)
{
}

UGameUserSettings* UOptionsWidget::GetGameUserSettings()
{
	if (GEngine) {
		return GEngine->GameUserSettings;
	}
	return nullptr;
}

void UOptionsWidget::SetResolution(int32 Width, int32 Height)
{
	FScreenResolution CurrentResolution = GetCurrentResolution();
	if (CurrentResolution.Width == Width && CurrentResolution.Height == Height) {
		return;
	}

	UGameUserSettings* Settings = GetGameUserSettings();
	Settings->SetScreenResolution(FIntPoint(Width, Height));
	Settings->ApplyResolutionSettings(false);
	Settings->ConfirmVideoMode();
	Settings->SaveSettings();
}

void UOptionsWidget::SetWindowMode(EWindowMode::Type Mode)
{
	UGameUserSettings* Settings = GetGameUserSettings();
	Settings->SetFullscreenMode(Mode);
	Settings->ApplyResolutionSettings(false);
	Settings->ConfirmVideoMode();
	Settings->SaveSettings();
}

TArray<FScreenResolution> UOptionsWidget::GetAvailableResolutions()
{
	TArray<FScreenResolution> Resolutions;
	FScreenResolutionArray AvailableResolutions;
	if (RHIGetAvailableResolutions(AvailableResolutions, true)) {
		for (const FScreenResolutionRHI& AvailableResolution : AvailableResolutions) {
			FScreenResolution Resolution;
			Resolution.Width = (int32)AvailableResolution.Width;
			Resolution.Height = (int32)AvailableResolution.Height;
			Resolutions.AddUnique(Resolution);
		}
	}
	return Resolutions;
}

FScreenResolution UOptionsWidget::GetCurrentResolution()
{
	FIntPoint ScreenResolution = GetGameUserSettings()->GetScreenResolution();
	FScreenResolution Resolution;
	Resolution.Width = ScreenResolution.X;
	Resolution.Height = ScreenResolution.Y;
	return Resolution;
}

EWindowMode::Type UOptionsWidget::IsFullscreen()
{
	return GetGameUserSettings()->GetFullscreenMode();
}

void UOptionsWidget::GetQualitySettings(int32& AntiAliasing, int32& Effects, int32& PostProcess, int32& Shadow, int32& Texture, int32& ViewDistance, int32& Foliage)
{
	AntiAliasing = GetAntiAliasingQuality();
	Effects = GetEffectsQuality();
	PostProcess = GetPostProcessQuality();
	Shadow = GetShadowQuality();
	Texture = GetTextureQuality();
	ViewDistance = GetViewDistanceQuality();
	Foliage = GetFoliageQuality();
}

int32 UOptionsWidget::GetViewDistanceQuality()
{
	return GetGameUserSettings()->GetViewDistanceQuality();
}

int32 UOptionsWidget::GetAntiAliasingQuality()
{
	return GetGameUserSettings()->GetAntiAliasingQuality();
}

int32 UOptionsWidget::GetShadowQuality()
{
	return GetGameUserSettings()->GetShadowQuality();
}

int32 UOptionsWidget::GetPostProcessQuality()
{
	return GetGameUserSettings()->GetPostProcessingQuality();
}

int32 UOptionsWidget::GetTextureQuality()
{
	return GetGameUserSettings()->GetTextureQuality();
}

int32 UOptionsWidget::GetEffectsQuality()
{
	return GetGameUserSettings()->GetVisualEffectQuality();
}

int32 UOptionsWidget::GetFoliageQuality()
{
	return GetGameUserSettings()->GetFoliageQuality();
}

void UOptionsWidget::SetViewDistanceQuality(int32 Quality)
{
	GetGameUserSettings()->SetViewDistanceQuality(Quality);
	GetGameUserSettings()->ApplyNonResolutionSettings();
	GetGameUserSettings()->SaveSettings();
}

void UOptionsWidget::SetAntiAliasingQuality(int32 Quality)
{
	GetGameUserSettings()->SetAntiAliasingQuality(Quality);
	GetGameUserSettings()->ApplyNonResolutionSettings();
	GetGameUserSettings()->SaveSettings();
}

void UOptionsWidget::SetShadowQuality(int32 Quality)
{
	GetGameUserSettings()->SetShadowQuality(Quality);
	GetGameUserSettings()->ApplyNonResolutionSettings();
	GetGameUserSettings()->SaveSettings();
}

void UOptionsWidget::SetPostProcessQuality(int32 Quality)
{
	GetGameUserSettings()->SetPostProcessingQuality(Quality);
	GetGameUserSettings()->ApplyNonResolutionSettings();
	GetGameUserSettings()->SaveSettings();
}

void UOptionsWidget::SetTextureQuality(int32 Quality)
{
	GetGameUserSettings()->SetTextureQuality(Quality);
	GetGameUserSettings()->ApplyNonResolutionSettings();
	GetGameUserSettings()->SaveSettings();
}

void UOptionsWidget::SetEffectsQuality(int32 Quality)
{
	GetGameUserSettings()->SetVisualEffectQuality(Quality);
	GetGameUserSettings()->ApplyNonResolutionSettings();
	GetGameUserSettings()->SaveSettings();
}

void UOptionsWidget::SetFoliageQuality(int32 Quality)
{
	GetGameUserSettings()->SetFoliageQuality(Quality);
	GetGameUserSettings()->ApplyNonResolutionSettings();
	GetGameUserSettings()->SaveSettings();
}
