// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "PlayerPartyCharacter.h"

#include "Globals.h"

#include "Blueprint/UserWidget.h"

#include "ArrowProjectile.h"
#include "BaseNPCCharacter.h"
#include "CharacterSheet.h"
#include "DataStructs.h"
#include "EquippableItem.h"
#include "GoldItem.h"
#include "Item.h"
#include "MagicProjectile.h"
#include "NNSGameInstance.h"
#include "NNSHUD.h"
#include "Pickup.h"
#include "Pool.h"
#include "QuestManager.h"
#include "StaticInteractable.h"
#include "ZoneChangerInteractable.h"

const float APlayerPartyCharacter::kPlayerWalkSpeedFactor = 0.4f;
const float APlayerPartyCharacter::kPlayerRunSpeed = 600.0f;
const float APlayerPartyCharacter::kPlayerCombatSpeedFactor = 0.5f;
const float APlayerPartyCharacter::kPlayerFleeSpeedFactor = 1.1f;
const float APlayerPartyCharacter::kPlayerBackwardsSpeedFactor = 0.4f;
const float APlayerPartyCharacter::kMaxTargetDistance = 3000.0f;
const float APlayerPartyCharacter::kMaxPickupDistance = 400.0f;

// Sets default values
APlayerPartyCharacter::APlayerPartyCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this character to call Tic() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FClassFinder<UUserWidget> ClassFinder(TEXT("WidgetBlueprint'/Game/Blueprints/UI/BP_EscapeMenu.BP_EscapeMenu_C'"));
	EscapeMenuClass = ClassFinder.Class;

	static ConstructorHelpers::FObjectFinder<USoundWave> BattleStartFinder(TEXT("SoundWave'/Game/Sounds/battle_start.battle_start'"));
	BattleStartSound = BattleStartFinder.Object;

	static ConstructorHelpers::FObjectFinder<USoundWave> BattleEndFinder(TEXT("SoundWave'/Game/Sounds/battle_end.battle_end'"));
	BattleEndSound = BattleEndFinder.Object;

	static ConstructorHelpers::FObjectFinder<USoundWave> DeathFinder(TEXT("SoundWave'/Game/Sounds/165331__ani-music__tubular-bell-of-death.165331__ani-music__tubular-bell-of-death'"));
	DeathSound = DeathFinder.Object;

	FirstPersonCameraComponent = ObjectInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(0, 0, 50.0f + BaseEyeHeight);
	FirstPersonCameraComponent->bUsePawnControlRotation = true;
	FirstPersonCameraComponent->PostProcessSettings.bOverride_MotionBlurAmount = true;
	FirstPersonCameraComponent->FieldOfView = UNNSGameInstance::GetFieldOfView();

	TorchComponent = ObjectInitializer.CreateDefaultSubobject<UPointLightComponent>(this, TEXT("Torch"));
	TorchComponent->SetupAttachment(GetCapsuleComponent());
	TorchComponent->Intensity = 0.0f;
	TorchComponent->bUseInverseSquaredFalloff = 0; // use attenuation radius for falloff
	TorchComponent->MinRoughness = 0.8f; // reduce specular
	TorchComponent->ShadowBias = 1.0f; // get rid of some shadow artifacts at distance

	NoiseComponent = ObjectInitializer.CreateDefaultSubobject<UPawnNoiseEmitterComponent>(this, TEXT("Noise"));
	NoiseComponent->bAutoActivate = true;

	bIsWalking = false;
	bIsSwimming = false;
	bWasMoving = false;
	FootstepInterval = 0.3f;

	HeldItem = nullptr;
	HighlightActorAttackable = nullptr;
	HighlightActorInteractable = nullptr;

	TickTags.Add(ActorTags::kIdentifiableTag);
	TickTags.Add(ActorTags::kInteractableTag);
	TickTags.Add(ActorTags::kAttackableTag);

	// Start the player in running mode
	GetCharacterMovement()->MaxWalkSpeed = kPlayerRunSpeed;
}

void APlayerPartyCharacter::SetEnableMotionBlur(bool bEnable)
{
	if (bEnable) {
		FirstPersonCameraComponent->PostProcessSettings.MotionBlurAmount = Cast<UNNSGameInstance>(GetGameInstance())->MotionBlurAmount;
	} else {
		FirstPersonCameraComponent->PostProcessSettings.MotionBlurAmount = 0.0f;
	}
}

void APlayerPartyCharacter::SaveLoadCommon(FArchive & Archive)
{
	Archive << bIsSwimming;
}

void APlayerPartyCharacter::Save(FArchive & Archive)
{
	FRotator Camera = Cast<APlayerController>(GetController())->GetControlRotation();
	Archive << Camera;

	Super::Save(Archive);

	bool IsHoldingItem = HeldItem != nullptr;
	Archive << IsHoldingItem;
	if (IsHoldingItem) {
		HeldItem->Save(Archive);
	}

	for (auto& Character : PartyMembers) {
		Character->Save(Archive);
	}
}

void APlayerPartyCharacter::Load(FArchive & Archive, bool bIgnoreTransforms)
{
	FRotator Camera;
	Archive << Camera;
	if (!bIgnoreTransforms) {
		Cast<APlayerController>(GetController())->SetControlRotation(Camera);
	}

	FTransform PlayerTransform;
	Archive << PlayerTransform;
	if (!bIgnoreTransforms) {
		SetActorTransform(PlayerTransform);
	}

	Super::Load(Archive);

	bool IsHoldingItem = false;
	Archive << IsHoldingItem;
	if (IsHoldingItem) {
		HeldItem = UItem::Load(Archive);
	} else {
		HeldItem = nullptr;
	}

	for (auto& Character : PartyMembers) {
		GetWorldTimerManager().ClearAllTimersForObject(Character);
		Character->Load(Archive);
	}

	UpdateVoice();

	OnEndWalk();
}

void APlayerPartyCharacter::UpdateVoice()
{
	DeathSounds.Empty();
	DamageSounds.Empty();
	AttackSounds.Empty();

	UNNSGameInstance* Instance = Cast<UNNSGameInstance>(GetGameInstance());

	for (const UCharacterSheet* Character : PartyMembers) {
		TAssetPtr<USoundWave> AttackRef(FStringAssetReference(TEXT("SoundWave'/Game/Sounds/60005_qubodup_sharp-swosh-18.60005_qubodup_sharp-swosh-18'")));
		AttackSounds.Add(Instance->GetSound(AttackRef));

		switch (Character->Voice) {
		case 0:
		{
			TAssetPtr<USoundWave> DamageRef(FStringAssetReference(TEXT("SoundWave'/Game/Sounds/voices/male_1_damage.male_1_damage'")));
			TAssetPtr<USoundWave> DeathRef(FStringAssetReference(TEXT("SoundWave'/Game/Sounds/voices/male_1_death.male_1_death'")));
			DamageSounds.Add(Instance->GetSound(DamageRef));
			DeathSounds.Add(Instance->GetSound(DeathRef));
			break;
		}
		case 1:
		{
			TAssetPtr<USoundWave> DamageRef(FStringAssetReference(TEXT("SoundWave'/Game/Sounds/voices/male_2_damage.male_2_damage'")));
			TAssetPtr<USoundWave> DeathRef(FStringAssetReference(TEXT("SoundWave'/Game/Sounds/voices/male_2_death.male_2_death'")));
			DamageSounds.Add(Instance->GetSound(DamageRef));
			DeathSounds.Add(Instance->GetSound(DeathRef));
			break;
		}
		case 2:
		{
			TAssetPtr<USoundWave> DamageRef(FStringAssetReference(TEXT("SoundWave'/Game/Sounds/voices/female_1_damage.female_1_damage'")));
			TAssetPtr<USoundWave> DeathRef(FStringAssetReference(TEXT("SoundWave'/Game/Sounds/voices/female_1_death.female_1_death'")));
			DamageSounds.Add(Instance->GetSound(DamageRef));
			DeathSounds.Add(Instance->GetSound(DeathRef));
			break;
		}
		case 3:
		{
			TAssetPtr<USoundWave> DamageRef(FStringAssetReference(TEXT("SoundWave'/Game/Sounds/voices/female_2_damage.female_2_damage'")));
			TAssetPtr<USoundWave> DeathRef(FStringAssetReference(TEXT("SoundWave'/Game/Sounds/voices/female_2_death.female_2_death'")));
			DamageSounds.Add(Instance->GetSound(DamageRef));
			DeathSounds.Add(Instance->GetSound(DeathRef));
			break;
		}
		}
	}
}

// Called when the game starts or when spawned
void APlayerPartyCharacter::BeginPlay()
{
	Super::BeginPlay();

	EscapeMenu = CreateWidget<UUserWidget>(Cast<APlayerController>(GetController()), EscapeMenuClass);

	// PartyMembers for the Player are stored in the GameInstance so they persist
	// across levels. We need to initialize the pointer every time we create
	// a Player Character.
	PartyMembers = Cast<UNNSGameInstance>(GetGameInstance())->PlayerPartyMembers;
	for (auto& Member : PartyMembers) {
		Member->OwnerCharacter = this;
	}
	UpdateVoice();

	Cast<UNNSGameInstance>(GetGameInstance())->QuestManager->PlayerParty = this;
	SetEnableMotionBlur(true);

	FTimerHandle Handle;
	GetWorld()->GetTimerManager().SetTimer(Handle, this, &APlayerPartyCharacter::VisibilityCheck, 0.5f, true);
}

// Called every frame
void APlayerPartyCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	AActor* NearestAttackable = nullptr;
	AActor* NearestInteractable = nullptr;

	AActor* NearestActor = nullptr;
	float NearestDistance = 0.0f;
	GetActorUnderMouse(TickTags, kMaxTargetDistance, NearestActor, NearestDistance);

	// First check for actor under mouse
	if (NearestActor != nullptr) {
		if (NearestActor->ActorHasTag(ActorTags::kAttackableTag)) {
			NearestAttackable = NearestActor;
		}
		if (NearestActor->ActorHasTag(ActorTags::kInteractableTag) && NearestDistance <= kMaxPickupDistance) {
			NearestInteractable = NearestActor;
		}
	}

	// Then find the nearest visible actor for each type
	if (NearestAttackable == nullptr) {
		GetNearestActorByTag(ActorTags::kEnemyTag, kMaxTargetDistance, NearestAttackable, NearestDistance);
	}
	if (NearestInteractable == nullptr) {
		GetNearestActorByTag(ActorTags::kInteractableTag, kMaxPickupDistance, NearestInteractable, NearestDistance);
	}

	const bool bBothSame = HighlightActorInteractable == HighlightActorAttackable;
	RemoveHighlighting(HighlightActorAttackable, NearestAttackable);
	RemoveHighlighting(HighlightActorInteractable, NearestInteractable);
	// If they were the same and one got cleared, reset the other one so that the highlighting gets added again
	if (HighlightActorInteractable == nullptr || HighlightActorAttackable == nullptr) {
		HighlightActorInteractable = nullptr;
		HighlightActorAttackable = nullptr;
	}
	AddHighlighting(HighlightActorInteractable, NearestInteractable, true);
	if (NearestAttackable != NearestInteractable) {
		// Interactable trumps attackable color, if they're identical
		AddHighlighting(HighlightActorAttackable, NearestAttackable);
	}

	// Check what material the player is walking on
	bool bSwimmingCheck = false;
	const bool bHasVelocity = !GetVelocity().IsZero();
	const bool bIsOnGround = GetCharacterMovement()->IsMovingOnGround();
	if (bIsOnGround) {
		FHitResult Hit(ForceInit);
		FCollisionQueryParams LineTraceParams(TEXT("LineTraceParams"), true);
		LineTraceParams.AddIgnoredActor(this);
		LineTraceParams.bReturnPhysicalMaterial = true;
		if (GetWorld()->LineTraceSingleByChannel(Hit, GetActorLocation(), GetActorLocation() + FVector(0.0f, 0.0f, -500.0f), ECollisionChannel::ECC_Visibility, LineTraceParams)) {
			if (Hit.PhysMaterial.IsValid()) {
				if (Hit.PhysMaterial->SurfaceType.GetValue() == EPhysicalSurface::SurfaceType1) {
					// Water
					bSwimmingCheck = true;
				}
			}
		}
	}

	if (!bSwimmingCheck && bIsSwimming) {
		StopSwimming();
	} else if (bSwimmingCheck && !bIsSwimming) {
		StartSwimming();
	}

	// Play footsteps
	if ((!bHasVelocity || !bIsOnGround) && bWasMoving) {
		GetWorld()->GetTimerManager().ClearTimer(FootstepHandle);
		bWasMoving = false;
	} else if ((bHasVelocity && bIsOnGround) && !bWasMoving) {
		GetWorld()->GetTimerManager().SetTimer(FootstepHandle, this, &APlayerPartyCharacter::TriggerFootstep, FootstepInterval * (2.0f - GetCharacterMovement()->MaxWalkSpeed / kPlayerRunSpeed), true, FootstepInterval / 4.0f);
		bWasMoving = true;
	}
}

void APlayerPartyCharacter::VisibilityCheck()
{
	bool bSomethingChanged = false;
	bool bFoundEnemy = false;
	TArray<FOverlapResult> AllActorsInRadius;
	if (GetWorld()->OverlapMultiByChannel(AllActorsInRadius, GetActorLocation(), FQuat::Identity, ECollisionChannel::ECC_Visibility, FCollisionShape::MakeSphere(kMaxTargetDistance))) {
		for (auto& Result : AllActorsInRadius) {
			AActor* Actor = Result.GetActor();
			if (Actor == nullptr) {
				continue;
			}

			const bool bIsEnemy = Actor->ActorHasTag(ActorTags::kEnemyTag);
			if (bIsEnemy) {
				bFoundEnemy = true;
			}

			if (Actor->ActorHasTag(ActorTags::kExcludeTag) || Actor->ActorHasTag(ActorTags::kVisibleTag)) {
				continue;
			}

			if (bIsEnemy || Actor->ActorHasTag(ActorTags::kFriendlyTag) || ((Actor->IsA<AStaticMeshActor>() || Actor->IsA<ADoor>() || Actor->IsA<AZoneChangerInteractable>() || Actor->IsA<APool>()) && GetDistanceTo(Actor) < 2000.0f)) {
				FCollisionQueryParams LineTraceParams(TEXT("LineTraceParams"), true);
				LineTraceParams.AddIgnoredComponent(GetCapsuleComponent());
				FHitResult Hit(ForceInit);
				FVector Origin, Extent;
				Actor->GetActorBounds(true, Origin, Extent);
				if (GetWorld()->LineTraceSingleByChannel(Hit, GetActorLocation(), Origin, ECollisionChannel::ECC_Visibility, LineTraceParams) && Hit.Actor == Actor) {
					Actor->Tags.Add(ActorTags::kVisibleTag);
					if (!bIsEnemy && !Actor->ActorHasTag(ActorTags::kFriendlyTag)) {
						Cast<UNNSGameInstance>(GetGameInstance())->MinimapCam->GetCaptureComponent2D()->HiddenActors.Remove(Actor);
					}
					bSomethingChanged = true;
				}
			}
		}
	}

	if (bSomethingChanged && Cast<UNNSGameInstance>(GetGameInstance())->MinimapCam.IsValid()) {
		Cast<UNNSGameInstance>(GetGameInstance())->MinimapCam->GetCaptureComponent2D()->CaptureScene();
	}

	if (!bFoundEnemy && HasStatus(EPartyStatusEffects::StatusInCombat)) {
		RemoveStatusEffect(EPartyStatusEffects::StatusInCombat);
	}
}

void APlayerPartyCharacter::TriggerFootstep()
{
	EPhysicalSurface Surface = EPhysicalSurface::SurfaceType_Default;
	FHitResult Hit(ForceInit);
	FCollisionQueryParams LineTraceParams(TEXT("LineTraceParams"), true);
	LineTraceParams.AddIgnoredActor(this);
	LineTraceParams.bReturnPhysicalMaterial = true;
	if (GetWorld()->LineTraceSingleByChannel(Hit, GetActorLocation(), GetActorLocation() + FVector(0.0f, 0.0f, -500.0f), ECollisionChannel::ECC_Visibility, LineTraceParams)) {
		if (Hit.PhysMaterial.IsValid()) {
			Surface = Hit.PhysMaterial->SurfaceType.GetValue();
		}
	}

	PlayFootstep(bFootstepLeft, Surface);
	NoiseComponent->MakeNoise(this, 1.0f, GetActorLocation());
	bFootstepLeft = !bFootstepLeft;
}

void APlayerPartyCharacter::PlayFootstep_Implementation(bool bIsLeftFoot, EPhysicalSurface Surface) {
}

void APlayerPartyCharacter::RemoveHighlighting(AActor*& Previous, AActor* Next)
{
	if (Previous != nullptr && Previous != Next) {
		TArray<UActorComponent*> Meshes = Previous->GetComponentsByClass(UMeshComponent::StaticClass());
		for (auto& FoundMesh : Meshes) {
			Cast<UMeshComponent>(FoundMesh)->SetRenderCustomDepth(false);
		}
		Previous = nullptr;
	}
}

void APlayerPartyCharacter::AddHighlighting(AActor*& Previous, AActor* Next, bool bAsInteractable)
{
	static const int32 kInteractableHighlight = 253;
	static const int32 kFriendlyHighlight = 254;
	static const int32 kEnemyHighlight = 255;

	if (Next != nullptr && Previous != Next) {
		Previous = Next;
		TArray<UActorComponent*> Meshes = Previous->GetComponentsByClass(UMeshComponent::StaticClass());
		for (auto& FoundMesh : Meshes) {
			UMeshComponent* MeshComponent = Cast<UMeshComponent>(FoundMesh);
			MeshComponent->SetRenderCustomDepth(true);
			if (bAsInteractable && Previous->ActorHasTag(ActorTags::kInteractableTag)) {
				MeshComponent->SetCustomDepthStencilValue(kInteractableHighlight);
			} else if (Previous->ActorHasTag(ActorTags::kFriendlyTag)) {
				MeshComponent->SetCustomDepthStencilValue(kFriendlyHighlight);
			} else if (Previous->ActorHasTag(ActorTags::kEnemyTag)) {
				MeshComponent->SetCustomDepthStencilValue(kEnemyHighlight);
			}
		}
	}
}

// Called to bind functionality to input
void APlayerPartyCharacter::SetupPlayerInputComponent(class UInputComponent* Input)
{
	Super::SetupPlayerInputComponent(Input);

	Input->BindAxis("MoveForward", this, &APlayerPartyCharacter::MoveForward);
	Input->BindAxis("MoveRight", this, &APlayerPartyCharacter::MoveRight);
	Input->BindAxis("TurnRight", this, &APlayerPartyCharacter::TurnRight);
	Input->BindAxis("TurnUp", this, &APlayerPartyCharacter::TurnUp);
	Input->BindAction("Jump", IE_Pressed, this, &APlayerPartyCharacter::OnStartJump);
	Input->BindAction("Jump", IE_Released, this, &APlayerPartyCharacter::OnEndJump);
	Input->BindAction("Walk", IE_Pressed, this, &APlayerPartyCharacter::OnStartWalk);
	Input->BindAction("Walk", IE_Released, this, &APlayerPartyCharacter::OnEndWalk);
	Input->BindAction("Attack", IE_Pressed, this, &APlayerPartyCharacter::Attack);
	// TODO: Make this a timer so it works with other keypresses
	Input->BindAction("Attack", IE_Repeat, this, &APlayerPartyCharacter::Attack);
	Input->BindAction("Cast", IE_Pressed, this, &APlayerPartyCharacter::OnCast);
	Input->BindAction("Recast", IE_Pressed, this, &APlayerPartyCharacter::OnRecast);
	Input->BindAction("Pass", IE_Pressed, this, &APlayerPartyCharacter::Pass);
	Input->BindAction("TurnMode", IE_Pressed, this, &APlayerPartyCharacter::ToggleTurnMode);
	Input->BindAction("ForceExitTurnMode", IE_Pressed, this, &APlayerPartyCharacter::ForceExitTurnMode);
	Input->BindAction("Interact", IE_Pressed, this, &APlayerPartyCharacter::Interact);
	Input->BindAction("Interact", IE_Repeat, this, &APlayerPartyCharacter::Interact);
	// Execute while paused allows hotkeys to work while in HUD
	Input->BindAction("Inventory", IE_Pressed, this, &APlayerPartyCharacter::ToggleInventory).bExecuteWhenPaused = true;
	Input->BindAction("Stats", IE_Pressed, this, &APlayerPartyCharacter::ToggleStats).bExecuteWhenPaused = true;
	Input->BindAction("Skills", IE_Pressed, this, &APlayerPartyCharacter::ToggleSkills).bExecuteWhenPaused = true;
	Input->BindAction("Quests", IE_Pressed, this, &APlayerPartyCharacter::ToggleQuests).bExecuteWhenPaused = true;
	Input->BindAction("Bestiary", IE_Pressed, this, &APlayerPartyCharacter::ToggleBestiary).bExecuteWhenPaused = true;
	Input->BindAction("Map", IE_Pressed, this, &APlayerPartyCharacter::ToggleMap).bExecuteWhenPaused = true;

	Input->BindAction("Player1", IE_Pressed, this, &APlayerPartyCharacter::OnPlayer1).bExecuteWhenPaused = true;
	Input->BindAction("Player2", IE_Pressed, this, &APlayerPartyCharacter::OnPlayer2).bExecuteWhenPaused = true;
	Input->BindAction("Player3", IE_Pressed, this, &APlayerPartyCharacter::OnPlayer3).bExecuteWhenPaused = true;
	Input->BindAction("Player4", IE_Pressed, this, &APlayerPartyCharacter::OnPlayer4).bExecuteWhenPaused = true;

	Input->BindAction("Quicksave", IE_Pressed, this, &APlayerPartyCharacter::Quicksave);
	Input->BindAction("Quickload", IE_Pressed, this, &APlayerPartyCharacter::Quickload);

	Input->BindAction("EScape", IE_Pressed, this, &APlayerPartyCharacter::Escape).bExecuteWhenPaused = true;
}

void APlayerPartyCharacter::TriggerParticlesAtCamera(UParticleSystem* Particles)
{
	FTransform ParticleTransform = FirstPersonCameraComponent->GetComponentTransform();
	ParticleTransform.SetLocation(ParticleTransform.GetLocation() + FirstPersonCameraComponent->GetForwardVector() * 80.0f);
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Particles, ParticleTransform);
}

uint32 APlayerPartyCharacter::GetGold() const
{
	return Cast<UNNSGameInstance>(GetGameInstance())->PlayerGold;
}

void APlayerPartyCharacter::RemoveGold(uint32 Gold) const
{
	uint32& PlayerGold = Cast<UNNSGameInstance>(GetGameInstance())->PlayerGold;
	if (Gold > 0 && PlayerGold - Gold >= 0) {
		PlayerGold -= Gold;
		Cast<UNNSGameInstance>(GetGameInstance())->PlaySoundEffect2D(GoldSound);
	}
}

void APlayerPartyCharacter::AddGold(uint32 Gold) const
{
	if (Gold > 0) {
		Cast<UNNSGameInstance>(GetGameInstance())->PlayerGold += Gold;
		Cast<UNNSGameInstance>(GetGameInstance())->PlaySoundEffect2D(GoldSound);
	}
}

uint32 APlayerPartyCharacter::ConvertCost(uint32 Gold, bool bIncrease) const
{
	int32 Level = 0;
	EMastery Mastery = EMastery::MasteryBasic;
	GetPartySkill(ESkills::SkillMerchant, Level, Mastery);
	return Gold * FMath::Clamp(1.0f - Level * ((int32)Mastery + 1) * 0.01f * (bIncrease ? -1 : 1), 0.5f, 1.5f);
}

bool APlayerPartyCharacter::AddItemToInventory(UItem* Item) const
{
	if (!Item) return false;

	if (Item->IsA<UGoldItem>()) {
		AddGold(Cast<UGoldItem>(Item)->Amount);
		return true;
	}

	int32 Level = 0;
	EMastery Mastery = EMastery::MasteryBasic;
	GetPartySkill(ESkills::SkillIdentifyItem, Level, Mastery);
	Item->Identify(Level, Mastery);

	for (int32 i = 0; i < PartyMembers.Num(); ++i) {
		if (PartyMembers[(ActiveCharacter + i) % PartyMembers.Num()]->AddItemToInventory(Item)) {
			const FString Log = FString::Printf(TEXT("%s picked up a %s."), *(PartyMembers[(ActiveCharacter + i) % PartyMembers.Num()]->Name), *(Item->GetName().ToString()));
			Cast<ANNSHUD>(Cast<APlayerController>(GetController())->GetHUD())->Log(Log);
			Cast<UNNSGameInstance>(GetGameInstance())->PlaySoundEffect2D(GoldSound);
			return true;
		}
	}
	const FString Log = FString::Printf(TEXT("No inventory space."));
	Cast<ANNSHUD>(Cast<APlayerController>(GetController())->GetHUD())->Log(Log);
	return false;
}

bool APlayerPartyCharacter::FindItem(const FName& ItemName, bool RemoveItem)
{
	if (HeldItem != nullptr && HeldItem->Name.IsEqual(ItemName)) {
		if (RemoveItem) {
			HeldItem = nullptr;
		}
		return true;
	} else {
		for (auto& Member : PartyMembers) {
			if (Member->FindItem(ItemName, RemoveItem)) {
				return true;
			}
		}
	}

	return false;
}

void APlayerPartyCharacter::GiveExperience(float Experience, bool Force)
{
	for (auto& Member : PartyMembers) {
		if (Force || !Member->IsDead()) {
			Member->GiveExperience(Experience);
		}
	}
}

void APlayerPartyCharacter::ToggleInventory()
{
	if (EscapeMenu->IsVisible()) return;
	Cast<ANNSHUD>(Cast<APlayerController>(GetController())->GetHUD())->ToggleHUDMenuType(EMenuType::MenuInventory);
}

void APlayerPartyCharacter::ToggleStats()
{
	if (EscapeMenu->IsVisible()) return;
	Cast<ANNSHUD>(Cast<APlayerController>(GetController())->GetHUD())->ToggleHUDMenuType(EMenuType::MenuStats);
}

void APlayerPartyCharacter::ToggleSkills()
{
	if (EscapeMenu->IsVisible()) return;
	Cast<ANNSHUD>(Cast<APlayerController>(GetController())->GetHUD())->ToggleHUDMenuType(EMenuType::MenuSkills);
}

void APlayerPartyCharacter::ToggleQuests()
{
	if (EscapeMenu->IsVisible()) return;
	Cast<ANNSHUD>(Cast<APlayerController>(GetController())->GetHUD())->ToggleHUDMenuType(EMenuType::MenuQuests);
}

void APlayerPartyCharacter::ToggleBestiary()
{
	if (EscapeMenu->IsVisible()) return;
	Cast<ANNSHUD>(Cast<APlayerController>(GetController())->GetHUD())->ToggleHUDMenuType(EMenuType::MenuBestiary);
}

void APlayerPartyCharacter::ToggleMap()
{
	if (EscapeMenu->IsVisible()) return;
	bool& bDrawMap = Cast<ANNSHUD>(Cast<APlayerController>(GetController())->GetHUD())->bDrawMap;
	bDrawMap = !bDrawMap;
}

void APlayerPartyCharacter::OnPlayer1()
{
	SwitchToPartyMember(0);
}

void APlayerPartyCharacter::OnPlayer2()
{
	SwitchToPartyMember(1);
}

void APlayerPartyCharacter::OnPlayer3()
{
	SwitchToPartyMember(2);
}

void APlayerPartyCharacter::OnPlayer4()
{
	SwitchToPartyMember(3);
}

void APlayerPartyCharacter::SwitchToPartyMember(int32 PartyMember)
{
	if (EscapeMenu->IsVisible()) return;
	if (Cast<ANNSHUD>(Cast<APlayerController>(GetController())->GetHUD())->MenuType == EMenuType::MenuSpellbook) return;
	// Allow switching only if in a menu or in realtime mode (and the character can act)
	if (!Cast<APlayerController>(GetController())->IsPaused() && (Cast<UNNSGameInstance>(GetGameInstance())->bIsInTurnMode || !PartyMembers[PartyMember]->bCanAct)) return;

	const bool bSwitchToSameCharacter = ActiveCharacter == PartyMember;
	ActiveCharacter = PartyMember;

	if (Cast<APlayerController>(GetController())->IsPaused()) {
		if (bSwitchToSameCharacter) {
			Cast<ANNSHUD>(Cast<APlayerController>(GetController())->GetHUD())->ToggleHUDMenuType(EMenuType::MenuNone);
		} else {
			Cast<ANNSHUD>(Cast<APlayerController>(GetController())->GetHUD())->RefreshHitboxes();
		}
	} else if (bSwitchToSameCharacter) {
		Cast<ANNSHUD>(Cast<APlayerController>(GetController())->GetHUD())->ToggleHUDMenuType(EMenuType::MenuInventory);
	}
}

void APlayerPartyCharacter::Escape()
{
	if (EscapeMenu->IsInViewport()) {
		EscapeMenu->RemoveFromParent();
		Cast<UNNSGameInstance>(GetGameInstance())->HUD->Pause(false);
	}  else if (!Cast<ANNSHUD>(Cast<APlayerController>(GetController())->GetHUD())->Escape()) {
		Cast<UNNSGameInstance>(GetGameInstance())->HUD->Pause(true);
		EscapeMenu->AddToViewport();
	}
}

void APlayerPartyCharacter::MoveForward(float Val)
{
	if (Cast<UNNSGameInstance>(GetGameInstance())->bIsInTurnMode) return;

	if (Controller != nullptr) {
		const bool bBackwards = Val < 0.0f;
		if (bBackwards != bIsMovingBackwards) {
			bIsMovingBackwards = bBackwards;
			UpdateWalkSpeed();
		}

		if (Val != 0.0f) {
			FRotator Rotation = Controller->GetControlRotation();
			if (GetCharacterMovement()->IsMovingOnGround() || GetCharacterMovement()->IsFalling()) {
				Rotation.Pitch = 0.0f;
			}

			const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::X);
			AddMovementInput(Direction, Val);
		}
	}
}

void APlayerPartyCharacter::MoveRight(float Val)
{
	if (Cast<UNNSGameInstance>(GetGameInstance())->bIsInTurnMode) return;

	if (Controller != nullptr && Val != 0.0f) {
		FRotator Rotation = Controller->GetControlRotation();
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::Y);
		AddMovementInput(Direction, Val);
	}
}

void APlayerPartyCharacter::TurnUp(float Val)
{
	AddControllerPitchInput(Val * Cast<UNNSGameInstance>(GetGameInstance())->MouseSensitivity);
}

void APlayerPartyCharacter::TurnRight(float Val)
{
	AddControllerYawInput(Val * Cast<UNNSGameInstance>(GetGameInstance())->MouseSensitivity);
}

void APlayerPartyCharacter::OnStartJump()
{
	if (Cast<UNNSGameInstance>(GetGameInstance())->bIsInTurnMode || bIsSwimming || IsJumpProvidingForce()) return;
	GetCharacterMovement()->JumpZVelocity = RegularJumpHeight;
	bPressedJump = true;
}

void APlayerPartyCharacter::OnEndJump()
{
	bPressedJump = false;
}

void APlayerPartyCharacter::OnStartWalk()
{
	bIsWalking = true;
	UpdateWalkSpeed();
}

void APlayerPartyCharacter::OnEndWalk()
{
	bIsWalking = false;
	UpdateWalkSpeed();
}

void APlayerPartyCharacter::UpdateWalkSpeed()
{
	const float OldSpeed = GetCharacterMovement()->MaxWalkSpeed;
	float Multiplier = 1.0f;
	if (HasStatus(EPartyStatusEffects::StatusFlee)) {
		Multiplier *= kPlayerFleeSpeedFactor;
	} else {
		if (bIsWalking) {
			Multiplier *= kPlayerWalkSpeedFactor;
		}
		if (bIsMovingBackwards && Cast<UNNSGameInstance>(GetGameInstance())->UseBackwardsWalkSpeed()) {
			Multiplier *= kPlayerBackwardsSpeedFactor;
		} else if (HasStatus(EPartyStatusEffects::StatusInCombat)) {
			Multiplier *= kPlayerCombatSpeedFactor;
		}
	}
	GetCharacterMovement()->MaxWalkSpeed = kPlayerRunSpeed * Multiplier;
	if (OldSpeed != GetCharacterMovement()->MaxWalkSpeed && GetWorld()->GetTimerManager().IsTimerActive(FootstepHandle)) {
		FTimerManager& Timer = GetWorld()->GetTimerManager();
		const float Remaining = Timer.GetTimerRemaining(FootstepHandle);
		Timer.ClearTimer(FootstepHandle);
		GetWorld()->GetTimerManager().SetTimer(FootstepHandle, this, &APlayerPartyCharacter::TriggerFootstep, FootstepInterval * (2.0f - Multiplier), true, Remaining);
	}
}

void APlayerPartyCharacter::ToggleTurnMode()
{
	UNNSGameInstance* GameInstance = Cast<UNNSGameInstance>(GetGameInstance());
	if (GameInstance->ToggleTurnMode()) {
		if (GameInstance->bIsInTurnMode) {
			if (BattleStartSound) {
				GameInstance->PlaySoundEffect2D(BattleStartSound);
			}
		} else {
			if (BattleEndSound) {
				GameInstance->PlaySoundEffect2D(BattleEndSound);
			}
		}
	}
}

void APlayerPartyCharacter::ForceExitTurnMode()
{
	Cast<UNNSGameInstance>(GetGameInstance())->SetTurnMode(false, true);
}

void APlayerPartyCharacter::Interact()
{
	if (HeldItem != nullptr) {
		APickup* Pickup = GetWorld()->SpawnActor<APickup>(HeldItem->PickupClass, FirstPersonCameraComponent->GetComponentLocation(), FirstPersonCameraComponent->GetComponentRotation(), FActorSpawnParameters());
		Pickup->Item = HeldItem;
		HeldItem = nullptr;
		Pickup->BoxComponent->AddImpulse(FirstPersonCameraComponent->GetForwardVector() * 1000.0f);
	} else {
		UWorld* const World = GetWorld();

		// Sphere collide to find all actors in front of player
		TArray<FOverlapResult> AllActorsInRadius;
		AActor* NearestActor = nullptr;
		float NearestDistance = 0;
		static const FName Flag = ActorTags::kInteractableTag;
		static const float Distance = kMaxPickupDistance;
		const bool bUnderMouse = GetActorUnderMouse(Flag, Distance, NearestActor, NearestDistance);
		if (bUnderMouse && NearestActor->ActorHasTag(ActorTags::kEnemyTag)) {
			AttackPrivate(Cast<ABaseEnemyCharacter>(NearestActor));
		} else if (bUnderMouse || GetNearestActorByTag(Flag, Distance, NearestActor, NearestDistance)) {
			if (NearestActor->ActorHasTag(ActorTags::kFriendlyTag)) {
				ABaseNPCCharacter* NPC = Cast<ABaseNPCCharacter>(NearestActor);
				if (NPC != nullptr) {
					if (NPC->Greeting != nullptr) {
						Cast<UNNSGameInstance>(GetGameInstance())->PlaySoundEffect2D(NPC->Greeting);
					}
					Cast<UNNSGameInstance>(GetGameInstance())->HUD->EnterDialog(NPC->DialogComponent);
				}
			} else if (NearestActor->ActorHasTag(ActorTags::kPickupableTag)) {
				Cast<APickup>(NearestActor)->AddToInventory(this);
			} else if (NearestActor->IsA<AStaticInteractable>()) {
				Cast<AStaticInteractable>(NearestActor)->Interact(this);
			}
		}
	}
}

void APlayerPartyCharacter::OnCast()
{
	if (PartyMembers[ActiveCharacter]->bCanAct) {
		Cast<UNNSGameInstance>(GetGameInstance())->HUD->ToggleHUDMenuType(EMenuType::MenuSpellbook);
	}
}

void APlayerPartyCharacter::OnRecast()
{
	if (PartyMembers[ActiveCharacter]->bCanAct) {
		const ESpells Spell = PartyMembers[ActiveCharacter]->LastSpellCast;
		const bool bValidSpell = Spell != ESpells::SpellTotalSpells;

		Cast<UNNSGameInstance>(GetGameInstance())->HUD->ToggleHUDMenuType(EMenuType::MenuSpellbook, !bValidSpell);
		if (bValidSpell) {
			const FName SpellName = FName(*EnumMaps::SpellInfo[Spell].Name);
			Cast<UNNSGameInstance>(GetGameInstance())->HUD->ClickedHitbox = SpellName;
			Cast<UNNSGameInstance>(GetGameInstance())->HUD->NotifyHitBoxRelease(SpellName);
		}
	}
}

void APlayerPartyCharacter::Attack()
{
	if (PartyMembers[ActiveCharacter]->bCanAct) {
		AActor* NearestActor = nullptr;
		float NearestDistance = 0;
		static const float Distance = kMaxTargetDistance;
		if (GetActorUnderMouse(ActorTags::kAttackableTag, Distance, NearestActor, NearestDistance) || GetNearestActorByTag(ActorTags::kEnemyTag, Distance, NearestActor, NearestDistance)) {
			AttackPrivate(Cast<ANNSCharacter>(NearestActor));
		} else {
			AttackPrivate();
		}
	}
}

void APlayerPartyCharacter::StartSwimming()
{
	bIsSwimming = true;
	for (auto& Member : PartyMembers) {
		Member->AddStatusEffect(EStatusEffects::StatusInWater);
	}
}

void APlayerPartyCharacter::StopSwimming()
{
	bIsSwimming = false;
	for (auto& Member : PartyMembers) {
		Member->RemoveStatusEffect(EStatusEffects::StatusInWater);
	}
}

void APlayerPartyCharacter::AddStatusEffect(EPartyStatusEffects Status, int32 Duration, int32 Strength)
{
	if (Status == EPartyStatusEffects::StatusInCombat && HasStatus(EPartyStatusEffects::StatusFlee)) {
		return;
	} else if (Status == EPartyStatusEffects::StatusFlee) {
		RemoveStatusEffect(EPartyStatusEffects::StatusInCombat);
	}
	const bool bWasInCombat = HasStatus(EPartyStatusEffects::StatusInCombat);

	Super::AddStatusEffect(Status, Duration, Strength);
	if (Status == EPartyStatusEffects::StatusTorchLight) {
		// Re-apply status effect (don't use function argument, because it might be weaker than current status effect)
		const EMastery StatusStrength = (EMastery)StatusEffects[Status].Strength;
		if (StatusStrength == EMastery::MasteryBasic) {
			TorchComponent->SetIntensity(5.0f);
			TorchComponent->SetAttenuationRadius(3000.0f);
		} else if (StatusStrength == EMastery::MasteryExpert) {
			TorchComponent->SetIntensity(7.0f);
			TorchComponent->SetAttenuationRadius(4000.0f);
		} else if (StatusStrength == EMastery::MasteryMaster) {
			TorchComponent->SetIntensity(9.0f);
			TorchComponent->SetAttenuationRadius(5000.0f);
		}
	} else if (Status == EPartyStatusEffects::StatusWizardsEye) {
		if ((EMastery)Strength == EMastery::MasteryMaster) {
			Cast<UNNSGameInstance>(GetGameInstance())->ShowWireframeColors(true);
		}
	} else if (Status == EPartyStatusEffects::StatusInCombat && !bWasInCombat) {
		UpdateWalkSpeed();
	}
}

void APlayerPartyCharacter::RemoveStatusEffect(EPartyStatusEffects Status)
{
	if (Status == EPartyStatusEffects::StatusTorchLight) {
		TorchComponent->SetIntensity(0.0f);
	} else if (Status == EPartyStatusEffects::StatusWizardsEye) {
		if ((EMastery)StatusEffects[Status].Strength == EMastery::MasteryMaster) {
			Cast<UNNSGameInstance>(GetGameInstance())->ShowWireframeColors(false);
		}
	}

	Super::RemoveStatusEffect(Status);
	if (Status == EPartyStatusEffects::StatusInCombat) {
		UpdateWalkSpeed();
	}
}

void APlayerPartyCharacter::DealDamage(UDamageData* Damage, bool DamageAll)
{
	Super::DealDamage(Damage, DamageAll);

	CheckDeath();
}

void APlayerPartyCharacter::DealDamage(UDamageData* Damage, UCharacterSheet* Character)
{
	Super::DealDamage(Damage, Character);

	CheckDeath();
}

void APlayerPartyCharacter::CheckDeath()
{
	for (const UCharacterSheet* Member : PartyMembers) {
		if (!Member->IsUnconscious()) return;
	}

	if (InputEnabled()) {
		DisableInput(Cast<APlayerController>(GetController()));

		if (DeathSound) {
			Cast<UNNSGameInstance>(GetGameInstance())->PlaySoundEffect2D(DeathSound);
		}

		Cast<UNNSGameInstance>(GetGameInstance())->Respawn();
	}
}

void APlayerPartyCharacter::AttackPrivate(ANNSCharacter* Target)
{
	UCharacterSheet* Character = PartyMembers[ActiveCharacter];
	EEquipType Slot = EEquipType::EquipMainHand;
	if (Character->bCanAct) {
		RemoveStatusEffect(EPartyStatusEffects::StatusFlee);
		if (Cast<UNNSGameInstance>(GetGameInstance())->bUseCombatMode) {
			const bool bAboutToTick = Cast<UNNSGameInstance>(GetGameInstance())->TimeOfDayManager->TimeRemainingUntilTick() <= 0.5f;
			AddStatusEffect(EPartyStatusEffects::StatusInCombat, bAboutToTick ? 3 : 2);
		}
		UWorld* const World = GetWorld();

		if (Target != nullptr && GetDistanceTo(Target) <= MaxMeleeDistance) {
			UDamageData* Damage = UDamageData::Create(EDamageTypes::DamagePhysical, PartyMembers[ActiveCharacter]->GetDamage(Slot), PartyMembers[ActiveCharacter]->GetStat(EStats::StatAccuracy),
													  EDamageDistance::DamageMelee, PartyMembers[ActiveCharacter]->Name, this);
			Target->DealDamage(Damage);
			NoiseComponent->MakeNoise(this, 1.0f, GetActorLocation());
			PlaySound(AttackSounds, ActiveCharacter);
		} else if (Character->Equipment[(int32)EEquipType::EquipRanged] != nullptr) {
			Slot = EEquipType::EquipRanged;
			int32 NumProjectiles = 1;
			const EEquipStyle Style = Character->Equipment[(int32)Slot]->Style;
			// Master bow gets 2 attacks
			if (Style == EEquipStyle::EquipBow && Character->IsMaster(EnumMaps::StyleToSkill[Style])) {
				++NumProjectiles;
			}

			for (int32 i = 0; i < NumProjectiles; ++i) {
				AProjectile* Projectile = SpawnProjectile(ArrowClass);
				if (Projectile == nullptr) return;

				Projectile->Damage = UDamageData::Create(EDamageTypes::DamagePhysical, PartyMembers[ActiveCharacter]->GetDamage(Slot), PartyMembers[ActiveCharacter]->GetStat(EStats::StatAccuracy),
														 EDamageDistance::DamageRanged, PartyMembers[ActiveCharacter]->Name, this);

				NoiseComponent->MakeNoise(this, 1.0f, GetActorLocation());
				LaunchProjectile(Projectile, Target);
			}
		} else {
			NoiseComponent->MakeNoise(this, 1.0f, GetActorLocation());
			PlaySound(AttackSounds, ActiveCharacter);
		}

		// TODO: should this start after any projectiles hit?
		PartyMembers[ActiveCharacter]->StartTurnTimer(PartyMembers[ActiveCharacter]->GetDelay(PartyMembers[ActiveCharacter]->GetAttackDelay(Slot)));
	}
}

bool APlayerPartyCharacter::CastSpell(ESpells Spell, UCharacterSheet* Target)
{
	PartyMembers[ActiveCharacter]->LastSpellCast = Spell;
	const bool Result = ANNSCharacter::CastSpell(Spell, Target);
	if (Result) {
		NoiseComponent->MakeNoise(this, 1.0f, GetActorLocation());
	}
	return Result;
}

FVector APlayerPartyCharacter::GetProjectileSpawnDirection() const
{
	return FirstPersonCameraComponent->GetForwardVector();
}

bool APlayerPartyCharacter::GetActorUnderMouse(FName Tag, float Distance, AActor*& Actor, float& ActorDistance)
{
	TArray<FName> TagList;
	TagList.Add(Tag);
	return GetActorUnderMouse(TagList, Distance, Actor, ActorDistance);
}

bool APlayerPartyCharacter::GetActorUnderMouse(TArray<FName> TagList, float Distance, AActor*& Actor, float& ActorDistance)
{
	// There is a function in the controller to get hit results under mouse, but it's not clear how to set the collision params
	// to AllDynamicObjects. So instead, just get the world location of the mouse and cast from there
	FVector WorldLocation, WorldDirection;
	Cast<APlayerController>(GetController())->DeprojectMousePositionToWorld(WorldLocation, WorldDirection);

	FHitResult Hit(ForceInit);
	FCollisionQueryParams LineTraceParams(TEXT("LineTraceParams"), true);
	LineTraceParams.AddIgnoredComponent(GetCapsuleComponent());
	if (GetWorld()->LineTraceSingleByChannel(Hit, WorldLocation, WorldLocation + WorldDirection * Distance, ECollisionChannel::ECC_Visibility, LineTraceParams)) {
		if (Hit.GetActor() != nullptr) {
			for (const FName& Tag : TagList) {
				if (Hit.GetActor()->ActorHasTag(Tag)) {
					Actor = Hit.GetActor();
					ActorDistance = Hit.Distance;
					return true;
				}
			}
		}
	}
	return false;
}

bool APlayerPartyCharacter::GetNearestActorByTag(FName Tag, float Distance, AActor*& NearestActor, float& NearestDistance)
{
	TArray<FName> TagList;
	TagList.Add(Tag);
	return GetNearestActorByTag(TagList, Distance, NearestActor, NearestDistance);
}

bool APlayerPartyCharacter::GetNearestActorByTag(TArray<FName> TagList, float Distance, AActor*& NearestActor, float& NearestDistance)
{
	UWorld* const World = GetWorld();

	// This will be used for our return value to see if we found any Actor
	NearestDistance = Distance + 1;
	// Sphere collide to find all dynamic actors in front of player
	TArray<FOverlapResult> AllActorsInRadius;
	if (World->OverlapMultiByChannel(AllActorsInRadius, GetActorLocation(), FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel1, FCollisionShape::MakeSphere(Distance))) {

		// Avoid re-initialization of some line trace vars
		const FVector StartTrace = FirstPersonCameraComponent->GetComponentLocation();
		FCollisionQueryParams LineTraceParams(TEXT("LineTraceParams"), true);
		LineTraceParams.AddIgnoredComponent(GetCapsuleComponent());

		for (auto& OverlapResult : AllActorsInRadius) {
			// Make sure we're only testing the collision capsule
			if (!OverlapResult.GetComponent()->IsA<UShapeComponent>()) continue;

			AActor* OtherActor = OverlapResult.GetActor();
			bool HasTag = false;
			for (const FName& Tag : TagList) {
				if (OtherActor->ActorHasTag(Tag)) {
					HasTag = true;
					break;
				}
			}
			const float DistanceToOtherActor = GetDistanceTo(OtherActor);
			if (DistanceToOtherActor < NearestDistance && HasTag) {
				const FVector OtherActorLocation = OtherActor->GetActorLocation();
				const FVector OtherActorUp = OtherActor->GetActorUpVector();

				// We'll line trace the center, top, left, right, and bottom of the Enemy's CapsuleComponent
				// from our player and see if any hit. If they do, then we'll check if the enemy location is
				// on screen. If it is, the enemy must be visible. Do this for every enemy, and pick the closest
				// one of the player, and that's who should be targeted.
				FVector OtherActorToPlayer = OtherActorLocation - GetActorLocation();
				OtherActorToPlayer.Normalize();

				// If the actor is behind the character, skip it
				if (FVector::DotProduct(FirstPersonCameraComponent->GetForwardVector(), OtherActorToPlayer) <= 0) continue;

				TArray<FVector> LocationsToCheck;
				// Center
				LocationsToCheck.Add(OtherActorLocation);

				// Check extents for each kind of collision component, depending on what the actor has
				UCapsuleComponent* Capsule = OtherActor->FindComponentByClass<UCapsuleComponent>();
				if (Capsule != nullptr) {
					// Right
					LocationsToCheck.Add(OtherActorLocation + FVector::CrossProduct(OtherActorUp, OtherActorToPlayer) * (Capsule->GetScaledCapsuleRadius() - 1));
					// Left
					LocationsToCheck.Add(OtherActorLocation + FVector::CrossProduct(OtherActorToPlayer, OtherActorUp) * (Capsule->GetScaledCapsuleRadius() - 1));
					// Up
					LocationsToCheck.Add(OtherActorLocation + OtherActorUp * (Capsule->GetScaledCapsuleHalfHeight() - 1));
					// Down
					LocationsToCheck.Add(OtherActorLocation + -OtherActorUp * (Capsule->GetScaledCapsuleHalfHeight() - 1));
				} else {
					UBoxComponent* Box = OtherActor->FindComponentByClass<UBoxComponent>();
					if (Box != nullptr) {
						FVector Extent = Box->GetScaledBoxExtent();
						// Near Top Left
						LocationsToCheck.Add(OtherActorLocation + Extent * 0.9f);
						// Far Bottom Right
						LocationsToCheck.Add(OtherActorLocation - Extent * 0.9f);
						Extent.Z *= -1;
						Extent.X *= -1;
						// Far Bottom Left
						LocationsToCheck.Add(OtherActorLocation + Extent * 0.9f);
						// Near Top Right
						LocationsToCheck.Add(OtherActorLocation - Extent * 0.9f);
					} else {
						USphereComponent* Sphere = OtherActor->FindComponentByClass<USphereComponent>();
						if (Sphere != nullptr) {
							// Right
							LocationsToCheck.Add(OtherActorLocation + FVector::CrossProduct(OtherActorUp, OtherActorToPlayer) * (Sphere->GetScaledSphereRadius() - 1));
							// Left
							LocationsToCheck.Add(OtherActorLocation + FVector::CrossProduct(OtherActorToPlayer, OtherActorUp) * (Sphere->GetScaledSphereRadius() - 1));
							// Up
							LocationsToCheck.Add(OtherActorLocation + OtherActorUp * (Sphere->GetScaledSphereRadius() - 1));
							// Down
							LocationsToCheck.Add(OtherActorLocation + -OtherActorUp * (Sphere->GetScaledSphereRadius() - 1));
						}
					}
				}

				// Do line trace for each position, if necessary
				for (int32 i = 0; i < LocationsToCheck.Num(); ++i) {
					FHitResult Hit(ForceInit);
					World->LineTraceSingleByChannel(Hit, StartTrace, LocationsToCheck[i], ECollisionChannel::ECC_Visibility, LineTraceParams);

					// Our trace hit the enemy, now let's see if it's on screen
					if (Hit.GetActor() == OtherActor && IsOnScreen(LocationsToCheck[i])) {
						NearestDistance = Hit.Distance;
						NearestActor = OtherActor;
						break;
					}
				}
			}
		}
	}

	return NearestDistance < Distance + 1;
}

bool APlayerPartyCharacter::IsOnScreen(FVector WorldLocation) const
{
	const APlayerController* const PlayerController = Cast<const APlayerController>(GetController());

	FVector2D ScreenLocation;
	PlayerController->ProjectWorldLocationToScreen(WorldLocation, ScreenLocation);

	int32 ScreenWidth = 0;
	int32 ScreenHeight = 0;
	PlayerController->GetViewportSize(ScreenWidth, ScreenHeight);

	int32 ScreenX = (int32)ScreenLocation.X;
	int32 ScreenY = (int32)ScreenLocation.Y;

	return ScreenX >= 0 && ScreenY >= 0 && ScreenX < ScreenWidth && ScreenY < ScreenHeight;
}

void APlayerPartyCharacter::Quicksave()
{
	Cast<UNNSGameInstance>(GetGameInstance())->SaveGameToFile(TEXT("quicksave"));
}

void APlayerPartyCharacter::Quickload()
{
	Cast<UNNSGameInstance>(GetGameInstance())->LoadGameFromFile(TEXT("quicksave"));
}
