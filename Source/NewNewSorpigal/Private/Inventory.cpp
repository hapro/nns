// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "Inventory.h"

#include "CharacterSheet.h"
#include "Item.h"
#include "NNSCharacter.h"

const int32 UInventory::kMaxWidth = 16;
const int32 UInventory::kMaxHeight = 8;
const int32 UInventory::kArrayLength = kMaxWidth * kMaxHeight;

UInventory::UInventory()
	: Super()
{
	Items.AddZeroed(kArrayLength);
	// This might be unnecessary, but just to keep things modern
	for (int32 i = 0; i < kArrayLength; ++i) {
		Items[i] = nullptr;
	}
}

void UInventory::Save(FArchive& Archive)
{
	int32 NumItems = PositionMap.Num();
	Archive << NumItems;
	for (auto& Item : PositionMap) {
		int32 X = PositionMap[Item.Key].Key;
		int32 Y = PositionMap[Item.Key].Value;
		Archive << X;
		Archive << Y;
		Item.Key->Save(Archive);
	}
}

void UInventory::Load(FArchive& Archive)
{
	for (int32 i = 0; i < kArrayLength; ++i) {
		Items[i] = nullptr;
	}
	PositionMap.Empty();

	int32 NumItems = 0;
	Archive << NumItems;
	for (int32 i = 0; i < NumItems; ++i) {
		int32 X = 0;
		int32 Y = 0;
		Archive << X;
		Archive << Y;

		PutItemInSlot(X, Y, UItem::Load(Archive));
	}
}

bool UInventory::AddItemToFirstOpenSlot(UItem* Item)
{
	// i and j are reversed, because we want to check horizontal before vertical
	for (int32 j = 0; j < kMaxHeight; ++j) {
		for (int32 i = 0; i < kMaxWidth; ++i) {
			// We can only drop when auto-adding, so don't need to check for swap
			if (IsSpaceEmpty(i, j, Item->IconWidth, Item->IconHeight) == nullptr) {
				if (PutItemInSlot(i, j, Item)) {
					return true;
				}
			}
		}
	}
	return false;
}

bool UInventory::RemoveItem(UItem* Item)
{
	if (!PositionMap.Contains(Item)) return false;

	for (int32 i = PositionMap[Item].Key; i < PositionMap[Item].Key + Item->IconWidth; ++i) {
		for (int32 j = PositionMap[Item].Value; j < PositionMap[Item].Value + Item->IconHeight; ++j) {
			Items[GetIndex(i, j)] = nullptr;
		}
	}
	PositionMap.Remove(Item);
	return true;
}

bool UInventory::CanDropOrSwap(int32 StartX, int32 StartY, UItem* Item, UItem*& SwapItem) const
{
	if (StartX + Item->IconWidth > kMaxWidth || StartY + Item->IconHeight > kMaxHeight) return false;

	int32 NumCollisions = 0;
	for (int32 i = StartX; i < StartX + Item->IconWidth; ++i) {
		for (int32 j = StartY; j < StartY + Item->IconHeight; ++j) {
			UItem* ItemAtLocation = GetItem(i, j);
			if (ItemAtLocation != nullptr && SwapItem != ItemAtLocation) {
				NumCollisions++;
				SwapItem = ItemAtLocation;
			}
		}
	}
	// We can drop with 0 collisions and swap with 1 collision. Check SwapItem for nullptr to distinguish
	return NumCollisions < 2;
}

bool UInventory::DropOrSwap(int32 StartX, int32 StartY, UItem* Item, UItem*& SwapItem)
{
	if (StartX + Item->IconWidth > kMaxWidth || StartY + Item->IconHeight > kMaxHeight) return false;

	const bool CanAct = CanDropOrSwap(StartX, StartY, Item, SwapItem);
	if (CanAct) {
		// If SwapItem is null, there were no collisions and we can safely drop
		if (SwapItem == nullptr) {
			return PutItemInSlot(StartX, StartY, Item);
		} else {
			// Store old item position in case inserting the new item fails somehow
			TPair<int32, int32> OldPosition = PositionMap[SwapItem];
			// If this fails something went terribly wrong
			if (RemoveItem(SwapItem)) {
				if (PutItemInSlot(StartX, StartY, Item)) {
					return true;
				} else {
					// Revert old item back in its original place. If this fails something went terribly wrong
					PutItemInSlot(OldPosition.Key, OldPosition.Value, SwapItem);
					return false;
				}
			}
		}
	}

	return false;
}

UItem* UInventory::IsSpaceEmpty(int32 StartX, int32 StartY, int32 Width, int32 Height) const
{
	if (StartX + Width > kMaxWidth || StartY + Height > kMaxHeight) return nullptr;

	for (int32 i = StartX; i < StartX + Width; ++i) {
		for (int32 j = StartY; j < StartY + Height; ++j) {
			UItem* ItemAtLocation = GetItem(i, j);
			if (ItemAtLocation != nullptr) {
				return ItemAtLocation;
			}
		}
	}
	return nullptr;
}

bool UInventory::PutItemInSlot(int32 StartX, int32 StartY, UItem* Item)
{
	if (StartX + Item->IconWidth > kMaxWidth || StartY + Item->IconHeight > kMaxHeight) return false;

	PositionMap.Add(Item, TPairInitializer<int32, int32>(StartX, StartY));
	for (int32 i = StartX; i < StartX + Item->IconWidth; ++i) {
		for (int32 j = StartY; j < StartY + Item->IconHeight; ++j) {
			// Set every slot this Item occupies with a pointer to the Item
			Items[GetIndex(i, j)] = Item;
		}
	}

	return true;
}
