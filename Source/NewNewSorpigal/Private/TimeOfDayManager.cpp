// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "TimeOfDayManager.h"

#include "CharacterSheet.h"
#include "NNSCharacter.h"

const float UTimeOfDayManager::TimeScale = 2.5f; // 2.5 is 1 game day per real hour
const int32 UTimeOfDayManager::MinutesPerTick = 1;

UTimeOfDayManager::UTimeOfDayManager()
{
	Hours = 12;
	Minutes = 0;
	TurnTimer = 0.0f;
	World = nullptr;
}

void UTimeOfDayManager::SetTime(int32 H, int32 M)
{
	Hours = H;
	Minutes = M;
}

void UTimeOfDayManager::Save(FArchive& Archive)
{
	const bool bTicking = IsTimerActive();
	if (bTicking) {
		TurnTimer = World->GetTimerManager().GetTimerElapsed(TickHandle);
	}
	SaveLoadCommon(Archive);
	if (bTicking) {
		TurnTimer = 0.0f;
	}
}

void UTimeOfDayManager::Load(FArchive& Archive)
{
	EndTimer();
	SaveLoadCommon(Archive);
}

void UTimeOfDayManager::SaveLoadCommon(FArchive& Archive)
{
	Archive << Hours;
	Archive << Minutes;
	Archive << TurnTimer;
}

void UTimeOfDayManager::StartTimer()
{
	if (World.IsValid()) {
		World->GetTimerManager().SetTimer(TickHandle, this, &UTimeOfDayManager::TickTimer, TimeScale, true, TurnTimer > 0.0f ? (TimeScale - TurnTimer) : -1.0f);
		TurnTimer = 0.0f;
	}
}

void UTimeOfDayManager::EndTimer()
{
	if (IsTimerActive()) {
		TurnTimer = World->GetTimerManager().GetTimerElapsed(TickHandle);
		World->GetTimerManager().ClearTimer(TickHandle);
	}
}

void UTimeOfDayManager::TickTimer()
{
	++MinutesSinceGameStart;
	Minutes += MinutesPerTick;
	if (Minutes >= 60) {
		Minutes -= 60;
		++Hours;
		if (Hours >= 24) {
			Hours -= 24;
		}
	}
	if (World.IsValid()) {
		TArray<AActor*> AllCharacters;
		UGameplayStatics::GetAllActorsOfClass(World.Get(), ANNSCharacter::StaticClass(), AllCharacters);
		for (auto& Actor : AllCharacters) {
			ANNSCharacter* Character = Cast<ANNSCharacter>(Actor);
			Character->TickStatusEffects(MinutesPerTick);
			for (auto& Member : Character->PartyMembers) {
				Member->TickStatusEffects(MinutesPerTick);
			}
		}
	}
}

void UTimeOfDayManager::AdvanceTurnTimer(float SecondsToAdvance)
{
	TurnTimer += SecondsToAdvance;
	while (TurnTimer >= TimeScale) {
		TurnTimer -= TimeScale;
		TickTimer();
	}
}

void UTimeOfDayManager::AdvanceTimeOfDay(float MinutesToAdvance)
{
	const float NumTicks = MinutesToAdvance / MinutesPerTick;
	for (int32 i = 0; i < NumTicks; ++i) {
		TickTimer();
	}
}

FString UTimeOfDayManager::GetAsString() const
{
	return FString::Printf(TEXT("%02d:%02d"), Hours, Minutes);
}

float UTimeOfDayManager::GetAsFloat() const
{
	float PartialMinutes = TurnTimer / TimeScale;
	if (IsTimerActive()) {
		PartialMinutes = World->GetTimerManager().GetTimerElapsed(TickHandle) / TimeScale;
	}
	return (float)Hours + ((float)Minutes + PartialMinutes * MinutesPerTick) / 60.0f;
}

float UTimeOfDayManager::TimeRemainingUntilTick() const
{
	if (IsTimerActive()) {
		return World->GetTimerManager().GetTimerRemaining(TickHandle) / TimeScale;
	} else {
		return TurnTimer / TimeScale;
	}
}
