// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "StatusPedestal.h"

#include "CharacterSheet.h"
#include "NNSCharacter.h"

AStatusPedestal::AStatusPedestal(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	static ConstructorHelpers::FObjectFinder<USoundWave> SoundFinder(TEXT("SoundWave'/Game/Sounds/220191__gameaudio__space-swoosh-brighter.220191__gameaudio__space-swoosh-brighter'"));
	Sound = SoundFinder.Object;

	Strength = 5;
	Duration = 30;
	Status = EStatusEffects::StatusTotalStatusEffects;
	PartyStatus = EPartyStatusEffects::StatusTotalPartyStatusEffects;

	OrbMesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("OrbMeshComp"));
	OrbMesh->BodyInstance.SetCollisionProfileName("BlockAll");
	OrbMesh->SetupAttachment(RootComponent);
}

void AStatusPedestal::Interact(ANNSCharacter* Character)
{
	if (Status != EStatusEffects::StatusTotalStatusEffects) {
		for (UCharacterSheet* Member : Character->PartyMembers) {
			Member->AddStatusEffect(Status, Duration, Strength);
		}
	}
	if (PartyStatus != EPartyStatusEffects::StatusTotalPartyStatusEffects) {
		Character->AddStatusEffect(PartyStatus, Duration, Strength);
	}

	if (Sound) {
		Cast<UNNSGameInstance>(GetGameInstance())->PlaySoundEffect2D(Sound);
	}
}
