// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "QuestItem.h"


UQuestItem::UQuestItem()
	: Super()
{
	bIsIdentified = true;
	Tier = 1;
}

UQuestItem* UQuestItem::Create(const FName& BaseName)
{
	UQuestItem* Item = NewObject<UQuestItem>();
	Item->Name = BaseName;
	Item->Init();

	return Item;
}

void UQuestItem::Init()
{
	// Name must be set before this is called
	const FQuestItemData* Data = UNNSGameInstance::QuestItemDataTable->FindRow<FQuestItemData>(Name, FString(TEXT("QuestItemData")));

	Icon = Data->Icon;
	IconWidth = Data->IconWidth;
	IconHeight = Data->IconHeight;
	PickupClass = Data->PickupClass;
}
