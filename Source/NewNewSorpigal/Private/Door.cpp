// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "Door.h"

#include "NNSCharacter.h"
#include "NNSGameInstance.h"
#include "NNSHUD.h"
#include "PlayerPartyCharacter.h"

ADoor::ADoor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;

	bOpenXPositive = true;
	SecondsToOpen = 1.0f;
	bIsOpen = false;
	OpeningSound = nullptr;
	bLoopOpeningSound = true;
	TimeElapsed = 0.0f;
	OpenPercent = 0.9f;
	Key = FName();
}

void ADoor::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (bIsOpen) {
		const FVector Direction = GetActorForwardVector() * (bOpenXPositive ? 1 : -1);
		SetActorLocation(GetActorLocation() + Direction * BoxComponent->GetScaledBoxExtent().X * (1.0f + OpenPercent) * DeltaSeconds / SecondsToOpen);
		TimeElapsed += DeltaSeconds;
		if (TimeElapsed >= SecondsToOpen) {
			SetActorTickEnabled(false);
		}

		if (Cast<UNNSGameInstance>(GetGameInstance())->bIsIndoors && Cast<UNNSGameInstance>(GetGameInstance())->MinimapCam.IsValid()) {
			Cast<UNNSGameInstance>(GetGameInstance())->MinimapCam->GetCaptureComponent2D()->CaptureScene();
		}
	}
}

void ADoor::SaveLoadCommon(FArchive& Archive)
{
	Archive << bIsOpen;
}

void ADoor::Load(FArchive& Archive)
{
	Super::Load(Archive);

	if (bIsOpen) {
		SetActorTickEnabled(false);
		const FVector Direction = GetActorForwardVector() * (bOpenXPositive ? 1 : -1);
		SetActorLocation(GetActorLocation() + Direction * BoxComponent->GetScaledBoxExtent().X * 1.9);
	}
}

void ADoor::Interact(ANNSCharacter* Character)
{
	if (!Key.IsNone()) {
		if (Character == nullptr || !Character->IsA<APlayerPartyCharacter>() || !Cast<APlayerPartyCharacter>(Character)->FindItem(Key, true)) {
			if (Character->IsA<APlayerPartyCharacter>()) {
				Cast<UNNSGameInstance>(GetGameInstance())->HUD->Log(FString::Printf(TEXT("%s is required to open this door"), *Key.ToString()));
			}
			return;
		} else {
			// Remove lock by clearing Key
			Key = FName();
		}
	}

	Super::Interact(Character);

	Tags.Remove(ActorTags::kInteractableTag);
	bIsOpen = true;
	if (OpeningSound != nullptr) {
		Cast<UNNSGameInstance>(GetGameInstance())->PlaySoundEffect3D(OpeningSound, GetActorLocation());
	}
}
