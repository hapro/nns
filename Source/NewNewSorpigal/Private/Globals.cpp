// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "Globals.h"

namespace ActorTags {
	const FName kFriendlyTag = TEXT("friendly");
	const FName kEnemyTag = TEXT("enemy");
	const FName kAttackableTag = TEXT("attack");
	const FName kPickupableTag = TEXT("pickup");
	const FName kInteractableTag = TEXT("interact");
	const FName kIdentifiableTag = TEXT("identify");
	const FName kExcludeTag = TEXT("exclude");
	const FName kVisibleTag = TEXT("visible");
}

namespace StatusEffectConstants {
	const int32 kMaxDuration = TNumericLimits<int32>::Max();
}

namespace EnumMaps {
	TMap<EDamageTypes, EStats> DamageResistances;
	TMap<EStats, EStatusEffects> StatusResistances;
	TMap<ETrapTypes, EDamageTypes> TrapDamages;
	TMap<ESkills, TArray<ESpells>> SchoolSpells;
	TMap<ESpells, FSpellInfo> SpellInfo;
	TMap<EMastery, FSkillCost> SkillCosts;
	TMap<EShopTypes, TArray<ESkills>> ShopSkills;
	TMap<EEquipStyle, ESkills> StyleToSkill;
}
