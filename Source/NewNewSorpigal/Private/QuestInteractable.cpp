// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "QuestInteractable.h"

#include "NNSGameInstance.h"
#include "QuestManager.h"

AQuestInteractable::AQuestInteractable(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

void AQuestInteractable::SaveLoadCommon(FArchive& Archive)
{
	Archive << Tags;
}

void AQuestInteractable::Interact(ANNSCharacter* Character)
{
	Super::Interact(Character);

	Cast<UNNSGameInstance>(GetGameInstance())->QuestManager->UpdateQuestsFromInteract(Quest);
	Tags.Empty();
}
