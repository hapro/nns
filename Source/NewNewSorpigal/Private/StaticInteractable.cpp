// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "StaticInteractable.h"

#include "Globals.h"

// Sets default values
AStaticInteractable::AStaticInteractable(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	BoxComponent = ObjectInitializer.CreateDefaultSubobject<UBoxComponent>(this, TEXT("BoxComp"));
	BoxComponent->BodyInstance.SetCollisionProfileName("StaticInteractable");
	RootComponent = BoxComponent;

	Mesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("MeshComp"));
	Mesh->BodyInstance.SetCollisionProfileName("BlockAll");
	Mesh->SetupAttachment(RootComponent);

	Tags.Add(ActorTags::kInteractableTag);
}

void AStaticInteractable::SaveLoadCommon(FArchive& Archive)
{
}

void AStaticInteractable::Save(FArchive& Archive)
{
	FString Name = GetName();
	Archive << Name;

	SaveLoadCommon(Archive);
}

void AStaticInteractable::Load(FArchive& Archive)
{
	SaveLoadCommon(Archive);
}

// Called when the game starts or when spawned
void AStaticInteractable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AStaticInteractable::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}
