// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "TempleBuilding.h"

#include "NNSGameInstance.h"
#include "NNSHUD.h"

ATempleBuilding::ATempleBuilding(const FObjectInitializer & ObjectInitializer)
	: Super(ObjectInitializer)
{
	Multiplier = 1.0f;
}

void ATempleBuilding::Interact(ANNSCharacter* Character)
{
	Super::Interact(Character);
	Cast<UNNSGameInstance>(GetGameInstance())->HUD->EnterBuilding(this);
}
