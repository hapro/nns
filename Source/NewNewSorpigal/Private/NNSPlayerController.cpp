// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "NNSPlayerController.h"

#include "NNSHUD.h"

ANNSPlayerController::ANNSPlayerController()
	: Super()
{
	bLockMouseInCenter = true;
	bEnableClickEvents = true;
}

void ANNSPlayerController::BeginPlay()
{
	FInputModeGameOnly Mode;
	Mode.SetConsumeCaptureMouseDown(false);
	SetInputMode(Mode);
}

bool ANNSPlayerController::InputKey(FKey Key, EInputEvent EventType, float AmountDepressed, bool bGamepad)
{
	const bool result = Super::InputKey(Key, EventType, AmountDepressed, bGamepad);
	if (PlayerInput)
	{
		// Right mouse button instead of left
		if (bEnableClickEvents && Key == EKeys::RightMouseButton)
		{
			FVector2D MousePosition;
			UGameViewportClient* ViewportClient = CastChecked<ULocalPlayer>(Player)->ViewportClient;
			if (ViewportClient && ViewportClient->GetMousePosition(MousePosition))
			{
				if (GetHUD())
				{
					// Call special right-click version of UpdateAndDispatchHitBoxClickEvents, because
					// otherwise we have no way to differentiate between mouse buttons in the function
					Cast<ANNSHUD>(GetHUD())->UpdateAndDispatchHitBoxRightClickEvents(MousePosition, EventType);
				}
			}
		}
	}

	return result;
}

void ANNSPlayerController::Tick(float DeltaSeconds)
{
	if (bLockMouseInCenter) {
		ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player);
		if (LocalPlayer && LocalPlayer->ViewportClient) {
			FVector2D ViewportSize;
			LocalPlayer->ViewportClient->GetViewportSize(ViewportSize);

			LocalPlayer->ViewportClient->Viewport->SetMouse(ViewportSize.X / 2, ViewportSize.Y / 2);
		}
	}
}
