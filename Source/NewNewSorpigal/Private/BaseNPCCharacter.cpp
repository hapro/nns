// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "BaseNPCCharacter.h"

#include "CharacterSheet.h"
#include "DialogComponent.h"

ABaseNPCCharacter::ABaseNPCCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	DialogComponent = ObjectInitializer.CreateDefaultSubobject<UDialogComponent>(this, TEXT("DialogComp"));

	Tags.Remove(ActorTags::kEnemyTag);
	Tags.Add(ActorTags::kFriendlyTag);
	Tags.Add(ActorTags::kInteractableTag);
}

void ABaseNPCCharacter::BeginPlay()
{
	Super::BeginPlay();

	for (auto& Member : PartyMembers) {
		Member->Name = GetName();
	}
	GetCharacterMovement()->MaxWalkSpeed = GetCharacterMovement()->MaxWalkSpeed / 2.0f;
}

void ABaseNPCCharacter::StartTurn()
{
	Super::StartTurn();

	// No AI yet, so just pass turn
	if (ActorHasTag(ActorTags::kFriendlyTag)) {
		for (auto& Member : PartyMembers) {
			if (Member->bCanAct) {
				Pass();
			}
		}
	}
}
