// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "QuestManager.h"

#include "NNSGameInstance.h"
#include "PlayerPartyCharacter.h"

UQuestManager::UQuestManager(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	static ConstructorHelpers::FObjectFinder<USoundWave> QuestTaskFinder(TEXT("SoundWave'/Game/Sounds/journal.journal'"));
	QuestTaskSound = QuestTaskFinder.Object;

	static ConstructorHelpers::FObjectFinder<USoundWave> QuestFinishedFinder(TEXT("SoundWave'/Game/Sounds/320657__rhodesmas__level-up-03.320657__rhodesmas__level-up-03'"));
	QuestFinishedSound = QuestFinishedFinder.Object;

	for (int32 i = 0; i < (int32)EQuestFlags::QuestTotalQuests + (int32)EBestiaryFlags::BestiaryTotalQuests; ++i) {
		Quests.Add(ObjectInitializer.CreateDefaultSubobject<UQuest>(this, *FString::Printf(TEXT("Quest%i"), i)));
		Quests[i]->Quest = i;
		QuestProgress.Emplace(i, 0);
	}
	GetQuest(EQuestFlags::QuestFreeMoney)->Name = TEXT("Welcoming Gift");
	AddQuestTask(EQuestFlags::QuestFreeMoney, UDialogQuestTask::Create(TEXT("Archibald")));
	AddQuestReward(EQuestFlags::QuestFreeMoney, 4000, 300);

	GetQuest(EQuestFlags::QuestSisterChat)->Name = TEXT("Sister Chat");
	AddQuestTask(EQuestFlags::QuestSisterChat, UDialogQuestTask::Create(TEXT("Elsie")));
	AddQuestTask(EQuestFlags::QuestSisterChat, UDialogQuestTask::Create(TEXT("Effie")));
	AddQuestTask(EQuestFlags::QuestSisterChat, UDialogQuestTask::Create(TEXT("Elsie")));
	AddQuestReward(EQuestFlags::QuestSisterChat, 2000, 300);

	GetQuest(EQuestFlags::QuestSpiderQueen)->Name = TEXT("Kill the Spider Queen");
	AddQuestTask(EQuestFlags::QuestSpiderQueen, UDialogQuestTask::Create(TEXT("Tavern")));
	AddQuestTask(EQuestFlags::QuestSpiderQueen, UKillQuestTask::Create(TEXT("SpiderQueen"), 1));
	AddQuestTask(EQuestFlags::QuestSpiderQueen, UDialogQuestTask::Create(TEXT("Tavern")));
	AddQuestReward(EQuestFlags::QuestSpiderQueen, 5000, 2000);

	GetQuest(EQuestFlags::QuestTrollKing)->Name = TEXT("Save the pigs");
	AddQuestTask(EQuestFlags::QuestTrollKing, UDialogQuestTask::Create(TEXT("Effie")));
	AddQuestTask(EQuestFlags::QuestTrollKing, UKillQuestTask::Create(TEXT("TrollKing"), 1));
	AddQuestTask(EQuestFlags::QuestTrollKing, UDialogQuestTask::Create(TEXT("Effie")));
	AddQuestReward(EQuestFlags::QuestTrollKing, 6000, 3000);

	GetQuest(EQuestFlags::QuestScavengerHuntStart)->Name = TEXT("Scavenger Hunt: Start");
	AddQuestTask(EQuestFlags::QuestScavengerHuntStart, UDialogQuestTask::Create(TEXT("Mayor")));
	AddQuestReward(EQuestFlags::QuestScavengerHuntStart, 0, 0);

	GetQuest(EQuestFlags::QuestScavengerHuntTown)->Name = TEXT("Scavenger Hunt: Town");
	AddQuestTask(EQuestFlags::QuestScavengerHuntTown, UFinishQuestTask::Create(EQuestFlags::QuestScavengerHuntStart));
	AddQuestTask(EQuestFlags::QuestScavengerHuntTown, UDialogQuestTask::Create(TEXT("Mayor"), FName(TEXT("TownKey")), true));
	AddQuestReward(EQuestFlags::QuestScavengerHuntTown, 1000, 1000);

	GetQuest(EQuestFlags::QuestScavengerHuntForest)->Name = TEXT("Scavenger Hunt: Forest");
	AddQuestTask(EQuestFlags::QuestScavengerHuntForest, UFinishQuestTask::Create(EQuestFlags::QuestScavengerHuntStart));
	AddQuestTask(EQuestFlags::QuestScavengerHuntForest, UDialogQuestTask::Create(TEXT("Mayor"), FName(TEXT("ForestKey")), true));
	AddQuestReward(EQuestFlags::QuestScavengerHuntForest, 1000, 1000);

	GetQuest(EQuestFlags::QuestScavengerHuntMountain)->Name = TEXT("Scavenger Hunt: Forest");
	AddQuestTask(EQuestFlags::QuestScavengerHuntMountain, UFinishQuestTask::Create(EQuestFlags::QuestScavengerHuntStart));
	AddQuestTask(EQuestFlags::QuestScavengerHuntMountain, UDialogQuestTask::Create(TEXT("Mayor"), FName(TEXT("MountainKey")), true));
	AddQuestReward(EQuestFlags::QuestScavengerHuntMountain, 1000, 1000);

	GetQuest(EQuestFlags::QuestScavengerHuntMaze)->Name = TEXT("Scavenger Hunt: Forest");
	AddQuestTask(EQuestFlags::QuestScavengerHuntMaze, UFinishQuestTask::Create(EQuestFlags::QuestScavengerHuntStart));
	AddQuestTask(EQuestFlags::QuestScavengerHuntMaze, UDialogQuestTask::Create(TEXT("Mayor"), FName(TEXT("MazeKey")), true));
	AddQuestReward(EQuestFlags::QuestScavengerHuntMaze, 1000, 1000);

	GetQuest(EQuestFlags::QuestMainTalkMayor)->Name = TEXT("Main Quest 1: Beginnings");
	AddQuestTask(EQuestFlags::QuestMainTalkMayor, UDialogQuestTask::Create(TEXT("Mayor")));
	AddQuestReward(EQuestFlags::QuestMainTalkMayor, 0, 50);

	GetQuest(EQuestFlags::QuestMainStatueTown)->Name = TEXT("Main Quest 2a: Town");
	AddQuestTask(EQuestFlags::QuestMainStatueTown, UFinishQuestTask::Create(EQuestFlags::QuestMainTalkMayor));
	AddQuestTask(EQuestFlags::QuestMainStatueTown, UInteractQuestTask::Create(TEXT("Find and investigate the angel statue in the town")));
	AddQuestReward(EQuestFlags::QuestMainStatueTown, 0, 100);

	GetQuest(EQuestFlags::QuestMainStatueMountain)->Name = TEXT("Main Quest 2b: Mountain");
	AddQuestTask(EQuestFlags::QuestMainStatueMountain, UFinishQuestTask::Create(EQuestFlags::QuestMainTalkMayor));
	AddQuestTask(EQuestFlags::QuestMainStatueMountain, UInteractQuestTask::Create(TEXT("Find and investigate the angel statue in the mountain")));
	AddQuestReward(EQuestFlags::QuestMainStatueMountain, 0, 500);

	GetQuest(EQuestFlags::QuestMainStatueForest)->Name = TEXT("Main Quest 2c: Forest");
	AddQuestTask(EQuestFlags::QuestMainStatueForest, UFinishQuestTask::Create(EQuestFlags::QuestMainTalkMayor));
	AddQuestTask(EQuestFlags::QuestMainStatueForest, UInteractQuestTask::Create(TEXT("Find and investigate the angel statue in the forest")));
	AddQuestReward(EQuestFlags::QuestMainStatueForest, 0, 1000);

	GetQuest(EQuestFlags::QuestMainStatueMaze)->Name = TEXT("Main Quest 2d: Maze");
	AddQuestTask(EQuestFlags::QuestMainStatueMaze, UFinishQuestTask::Create(EQuestFlags::QuestMainTalkMayor));
	AddQuestTask(EQuestFlags::QuestMainStatueMaze, UInteractQuestTask::Create(TEXT("Find and investigate the angel statue in the maze")));
	AddQuestReward(EQuestFlags::QuestMainStatueMaze, 0, 1500);

	GetQuest(EQuestFlags::QuestMainDemonDoor)->Name = TEXT("Main Quest 3: Nosferatu");
	AddQuestTask(EQuestFlags::QuestMainDemonDoor, UFinishQuestTask::Create(EQuestFlags::QuestMainStatueTown));
	AddQuestTask(EQuestFlags::QuestMainDemonDoor, UFinishQuestTask::Create(EQuestFlags::QuestMainStatueMountain));
	AddQuestTask(EQuestFlags::QuestMainDemonDoor, UFinishQuestTask::Create(EQuestFlags::QuestMainStatueForest));
	AddQuestTask(EQuestFlags::QuestMainDemonDoor, UFinishQuestTask::Create(EQuestFlags::QuestMainStatueMaze));
	AddQuestTask(EQuestFlags::QuestMainDemonDoor, UKillQuestTask::Create(TEXT("Nosferatu"), 1));
	AddQuestReward(EQuestFlags::QuestMainDemonDoor, 100000, 100000);

	GetBestiaryQuest(EBestiaryFlags::BestiarySpiders)->Name = TEXT("Spiders");
	AddBestiaryTask(EBestiaryFlags::BestiarySpiders, UKillQuestTask::Create(TEXT("Spiderling"), 7));
	AddBestiaryTask(EBestiaryFlags::BestiarySpiders, UKillQuestTask::Create(TEXT("Spider"), 5));
	AddBestiaryTask(EBestiaryFlags::BestiarySpiders, UKillQuestTask::Create(TEXT("GiantSpider"), 3));
	AddBestiaryReward(EBestiaryFlags::BestiarySpiders, 1000, 500);

	GetBestiaryQuest(EBestiaryFlags::BestiaryGoblins)->Name = TEXT("Goblins");
	AddBestiaryTask(EBestiaryFlags::BestiaryGoblins, UKillQuestTask::Create(TEXT("Goblin"), 7));
	AddBestiaryTask(EBestiaryFlags::BestiaryGoblins, UKillQuestTask::Create(TEXT("GoblinSoldier"), 5));
	AddBestiaryTask(EBestiaryFlags::BestiaryGoblins, UKillQuestTask::Create(TEXT("GoblinOverlord"), 3));
	AddBestiaryReward(EBestiaryFlags::BestiaryGoblins, 1500, 750);

	GetBestiaryQuest(EBestiaryFlags::BestiaryDragonflies)->Name = TEXT("Dragonflies");
	AddBestiaryTask(EBestiaryFlags::BestiaryDragonflies, UKillQuestTask::Create(TEXT("DragonflyHatchling"), 7));
	AddBestiaryTask(EBestiaryFlags::BestiaryDragonflies, UKillQuestTask::Create(TEXT("Dragonfly"), 5));
	AddBestiaryTask(EBestiaryFlags::BestiaryDragonflies, UKillQuestTask::Create(TEXT("FireDragonfly"), 3));
	AddBestiaryReward(EBestiaryFlags::BestiaryDragonflies, 2000, 1000);

	GetBestiaryQuest(EBestiaryFlags::BestiaryTrolls)->Name = TEXT("Trolls");
	AddBestiaryTask(EBestiaryFlags::BestiaryTrolls, UKillQuestTask::Create(TEXT("TrollGrunt"), 7));
	AddBestiaryTask(EBestiaryFlags::BestiaryTrolls, UKillQuestTask::Create(TEXT("TrollSergeant"), 5));
	AddBestiaryTask(EBestiaryFlags::BestiaryTrolls, UKillQuestTask::Create(TEXT("TrollCaptain"), 3));
	AddBestiaryReward(EBestiaryFlags::BestiaryTrolls, 3000, 1500);

	GetBestiaryQuest(EBestiaryFlags::BestiaryVampires)->Name = TEXT("Vampires");
	AddBestiaryTask(EBestiaryFlags::BestiaryVampires, UKillQuestTask::Create(TEXT("Vampire"), 7));
	AddBestiaryTask(EBestiaryFlags::BestiaryVampires, UKillQuestTask::Create(TEXT("ElderVampire"), 5));
	AddBestiaryTask(EBestiaryFlags::BestiaryVampires, UKillQuestTask::Create(TEXT("AncientVampire"), 3));
	AddBestiaryReward(EBestiaryFlags::BestiaryVampires, 6000, 3000);
}

void UQuestManager::SaveLoadCommon(FArchive & Archive)
{
	Archive << QuestProgress;

	for (auto& Quest : Quests) {
		Quest->SaveLoadCommon(Archive);
	}
}

void UQuestManager::Save(FArchive & Archive)
{
	SaveLoadCommon(Archive);
}

void UQuestManager::Load(FArchive & Archive)
{
	SaveLoadCommon(Archive);
}

void UQuestManager::UpdateQuestFromDialog(EQuestFlags Quest, const FString& CharacterName)
{
	if (IsCompleted(Quest)) return;

	UQuestTask* Task = GetQuest(Quest)->Tasks[QuestProgress[(int32)Quest]];
	if (Task->IsA<UDialogQuestTask>()) {
		UDialogQuestTask* DialogTask = Cast<UDialogQuestTask>(Task);
		if (DialogTask->CharacterName.Equals(CharacterName)) {
			bool HasItem = true;
			if (!DialogTask->ItemName.IsNone()) {
				HasItem = PlayerParty->FindItem(DialogTask->ItemName, DialogTask->bDestroyItem);
			}

			if (HasItem) {
				Task->bIsCompleted = true;
				UpdateQuestState((int32)Quest);
			}
		}
	}
}

void UQuestManager::UpdateQuestsFromKill(const FString& CharacterName)
{
	for (auto& Quest : Quests) {
		if (IsCompleted(Quest->Quest)) continue;

		for (auto& Task : Quest->Tasks) {
			if (Task->bIsCompleted) continue;

			if (Task->IsA<UKillQuestTask>()) {
				UKillQuestTask* KillTask = Cast<UKillQuestTask>(Task);
				if (KillTask->CharacterName.Equals(CharacterName)) {
					--KillTask->AmountToKill;
					if (KillTask->AmountToKill == 0) {
						KillTask->bIsCompleted = true;
						UpdateQuestState(Quest->Quest);
					}
				}
			}
		}
	}
}

void UQuestManager::UpdateQuestsFromFinish(EQuestFlags QuestFlag)
{
	for (auto& Quest : Quests) {
		if (IsCompleted(Quest->Quest)) continue;

		for (auto& Task : Quest->Tasks) {
			if (Task->bIsCompleted) continue;

			if (Task->IsA<UFinishQuestTask>()) {
				UFinishQuestTask* FinishTask = Cast<UFinishQuestTask>(Task);
				if (FinishTask->QuestFlag == QuestFlag) {
					FinishTask->bIsCompleted = true;
					UpdateQuestState(Quest->Quest);
				}
			}
		}
	}
}

void UQuestManager::UpdateQuestsFromInteract(EQuestFlags QuestFlag)
{
	if (IsCompleted(QuestFlag)) return;

	for (auto& Task : GetQuest(QuestFlag)->Tasks) {
		if (Task->bIsCompleted) continue;

		if (Task->IsA<UInteractQuestTask>()) {
			Task->bIsCompleted = true;
			UpdateQuestState((int32)QuestFlag);
		}
	}
}

void UQuestManager::UpdateQuestState(int32 Quest)
{
	bool bHadProgress = false;
	bool bWasCompleted = false;

	while (Quests[Quest]->Tasks[QuestProgress[Quest]]->bIsCompleted) {
		++QuestProgress[Quest];
		bHadProgress = true;

		if (IsCompleted(Quest)) {
			bWasCompleted = true;
			PlayerParty->AddGold(QuestRewards[Quest].Gold);
			PlayerParty->GiveExperience(QuestRewards[Quest].Experience, true);
			QuestFinishedDelegate.Broadcast((EQuestFlags)Quest);
			if (Quest < (int32)EQuestFlags::QuestTotalQuests) {
				UpdateQuestsFromFinish((EQuestFlags)Quest);
			}
			break;
		}
	}

	if (bHadProgress) {
		if (bWasCompleted) {
			if (QuestFinishedSound) {
				GameInstance->PlaySoundEffect2D(QuestFinishedSound);
			}
		} else {
			if (QuestTaskSound) {
				GameInstance->PlaySoundEffect2D(QuestTaskSound);
			}
		}
	}
}

bool UQuestManager::IsCurrentOrPreviousStepCharacter(EQuestFlags Quest, const FString& CharacterName, int32 FlagNum) const
{
	if (IsCompleted((int32)Quest)) return false;

	const int32 CurrentTask = QuestProgress[(int32)Quest];
	for (int32 i = 0; i <= CurrentTask; ++i) {
		UQuestTask* Task = GetQuest(Quest)->Tasks[i];
		if (!Task->IsA<UDialogQuestTask>()) continue;

		if (Cast<UDialogQuestTask>(Task)->CharacterName.Equals(CharacterName)) {
			if (i >= CurrentTask - 1 && FlagNum == 0) {
				return true;
			}
			--FlagNum;
		}
	}
	return false;
}

bool UQuestManager::IsCharacterDialogFinished(EQuestFlags Quest, const FString& CharacterName, int32 FlagNum) const
{
	if (IsCompleted((int32)Quest)) return true;

	const int32 CurrentTask = QuestProgress[(int32)Quest];
	for (int32 i = 0; i <= CurrentTask; ++i) {
		UQuestTask* Task = GetQuest(Quest)->Tasks[i];
		if (!Task->IsA<UDialogQuestTask>()) continue;

		if (Cast<UDialogQuestTask>(Task)->CharacterName.Equals(CharacterName)) {
			if (FlagNum == 0) {
				return Task->bIsCompleted;
			}
			--FlagNum;
		}
	}
	return false;
}

bool UQuestManager::IsCompleted(int32 Quest) const
{
	return QuestProgress[Quest] >= Quests[Quest]->Tasks.Num();
}

void UQuestManager::AddQuestTask(EQuestFlags Quest, UQuestTask* Task)
{
	GetQuest(Quest)->Tasks.Add(Task);
}

void UQuestManager::AddQuestReward(EQuestFlags Quest, int32 Gold, int32 Experience)
{
	QuestRewards.Emplace((int32)Quest, FQuestReward(Gold, Experience));
}

UQuest* UQuestManager::GetQuest(EQuestFlags Quest) const
{
	return Quests[(int32)Quest];
}

const FQuestReward& UQuestManager::GetQuestReward(EQuestFlags Quest) const
{
	return QuestRewards[(int32)Quest];
}

bool UQuestManager::IsCompleted(EQuestFlags Quest) const
{
	return IsCompleted((int32)Quest);
}

int32 UQuestManager::GetCurrentTask(EQuestFlags Quest) const
{
	return QuestProgress[(int32)Quest];
}

void UQuestManager::AddBestiaryTask(EBestiaryFlags Quest, UQuestTask* Task)
{
	GetBestiaryQuest(Quest)->Tasks.Add(Task);
}

void UQuestManager::AddBestiaryReward(EBestiaryFlags Quest, int32 Gold, int32 Experience)
{
	QuestRewards.Emplace((int32)EQuestFlags::QuestTotalQuests + (int32)Quest, FQuestReward(Gold, Experience));
}

UQuest* UQuestManager::GetBestiaryQuest(EBestiaryFlags Quest) const
{
	return Quests[(int32)EQuestFlags::QuestTotalQuests + (int32)Quest];
}

const FQuestReward& UQuestManager::GetBestiaryReward(EBestiaryFlags Quest) const
{
	return QuestRewards[(int32)EQuestFlags::QuestTotalQuests + (int32)Quest];
}

bool UQuestManager::IsCompleted(EBestiaryFlags Quest) const
{
	return IsCompleted((int32)EQuestFlags::QuestTotalQuests + (int32)Quest);
}

TArray<FText> UQuestManager::GetQuestText(bool bCompletedQuests, bool bUseBestiary) const
{
	TArray<FText> QuestText;
	if (bUseBestiary) {
		for (int32 i = 0; i < (int32)EBestiaryFlags::BestiaryTotalQuests; ++i) {
			const bool bIsCompleted = IsCompleted((EBestiaryFlags)i);
			if (bCompletedQuests != bIsCompleted) continue;

			UQuest* Quest = GetBestiaryQuest((EBestiaryFlags)i);
			const FQuestReward& Reward = GetBestiaryReward((EBestiaryFlags)i);

			FString Text = FString::Printf(TEXT("%s (%i gold, %i experience):"), *Quest->Name, Reward.Gold, Reward.Experience);

			for (const UQuestTask* Task : Quest->Tasks) {
				const UKillQuestTask* KillTask = Cast<const UKillQuestTask>(Task);
				Text.Append(FString::Printf(TEXT("\n    %s: %i / %i"), *KillTask->CharacterName, (KillTask->TotalToKill - KillTask->AmountToKill), KillTask->TotalToKill));
			}
			QuestText.Add(FText::FromString(Text));
		}
	} else {
		for (int32 i = 0; i < (int32)EQuestFlags::QuestTotalQuests; ++i) {
			const bool bIsCompleted = IsCompleted((EQuestFlags)i);
			if (bCompletedQuests != bIsCompleted) continue;

			UQuest* Quest = GetQuest((EQuestFlags)i);

			// Find the first non-finish task. If all the finish tasks are done, then check
			// if the next task is dialog. If it's not, the quest is started. If it is, the
			// quest is only started if the dialog task is finished.
			int32 FirstTask = 0;
			bool bInitialCompleted = true;
			for (int32 j = 0; j < Quest->Tasks.Num(); ++j) {
				if (Quest->Tasks[j]->IsA<UFinishQuestTask>()) {
					++FirstTask;
					if (!Quest->Tasks[j]->bIsCompleted) {
						bInitialCompleted = false;
						break;
					}
				} else {
					break;
				}
			}
			if (!bInitialCompleted || (Quest->Tasks[FirstTask]->IsA<UDialogQuestTask>() && !Quest->Tasks[FirstTask]->bIsCompleted)) continue;

			const FQuestReward& Reward = GetQuestReward((EQuestFlags)i);

			FString Text = TEXT("");
			if (Quest->Tasks[FirstTask]->IsA<UDialogQuestTask>()) {
				Text = FString::Printf(TEXT("%s (received from %s)"), *Quest->Name, *Cast<UDialogQuestTask>(Quest->Tasks[FirstTask])->CharacterName);
			} else {
				Text = FString::Printf(TEXT("%s"), *Quest->Name);
			}
			Text.Append(FString::Printf(TEXT(" (%i gold, %i experience):"), Reward.Gold, Reward.Experience));

			for (int32 j = FirstTask; j < Quest->Tasks.Num(); ++j) {
				// Don't print out first task if it's a dialog task, or finish tasks at all
				if (Quest->Tasks[j]->IsA<UFinishQuestTask>() || (Quest->Tasks[j]->IsA<UDialogQuestTask>() && j == FirstTask)) continue;

				const bool bIsCurrentTask = j == GetCurrentTask((EQuestFlags)i);
				const bool bIsTaskCompleted = Quest->Tasks[j]->bIsCompleted;
				Text.Append(FString::Printf(TEXT("\n    %s: %s"), (bIsCurrentTask ? TEXT("In Progress") : (bIsTaskCompleted ? TEXT("Completed") : TEXT("Not Started"))), *Quest->Tasks[j]->ToString()));
			}
			QuestText.Add(FText::FromString(Text));
		}
	}
	return QuestText;
}
