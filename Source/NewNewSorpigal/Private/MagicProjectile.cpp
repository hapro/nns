// Fill out your copyright notice in the Description page of Project Settings.

#include "NewNewSorpigal.h"
#include "MagicProjectile.h"

#include "NNSCharacter.h"
#include "NNSGameInstance.h"

AMagicProjectile::AMagicProjectile(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereMesh(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));
	Mesh->SetStaticMesh(SphereMesh.Object);

	HitParticles = nullptr;
	HitSound = nullptr;
}

void AMagicProjectile::HitCallback(AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	PlayHitParticleAndSound();
	Super::HitCallback(OtherActor, OtherComp, NormalImpulse, Hit);
}

void AMagicProjectile::OnOverlapMesh(ANNSCharacter* HitCharacter)
{
	PlayHitParticleAndSound();
	Super::OnOverlapMesh(HitCharacter);
}

void AMagicProjectile::PlayHitParticleAndSound()
{
	if (HitParticles != nullptr) {
		// Spawn emitter separately from this actor, because the actor will be destroyed instantly
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitParticles, RootComponent->GetComponentTransform());
	}
	if (HitSound != nullptr) {
		Cast<UNNSGameInstance>(GetGameInstance())->PlaySoundEffect3D(HitSound, GetActorLocation());
	}
}
